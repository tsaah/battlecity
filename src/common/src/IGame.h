#pragma once

class IGame {
public:
    virtual ~IGame() = default;

    virtual bool init() = 0;
    virtual bool update() = 0;
};

extern "C"
{
    __declspec(dllexport) IGame* createGameInstance();
}