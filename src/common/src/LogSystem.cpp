#include "LogSystem.h"

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

const std::array<const char*, 6> LogSystem::LOG_LEVEL_STRINGS = { "F", "E", "W", "I", "D", "T" };

LogSystem& LogSystem::instance() {
    static LogSystem i;
    return i;
}

void LogSystem::log(LogLevel level, const char* message, ...) {
    const auto isError = level < LogLevel::LVL_WARN;

    const i32 messageLength = 32000; // TODO: fix that

    char outBuffer[messageLength];
    memset(outBuffer, 0, sizeof(outBuffer));

    // __builtin_va_list argPtr;
    va_list argPtr;
    va_start(argPtr, message);
    vsnprintf(outBuffer, messageLength, message, argPtr);
    va_end(argPtr);

    char outBuffer2[messageLength];
    sprintf(outBuffer2, "%s: %s\n", LOG_LEVEL_STRINGS[level], outBuffer);

    if (isError) {
        console_.writeError(outBuffer2, level);
    } else {
        console_.write(outBuffer2, level);
    }
}

void LogSystem::reportAssertionFailure(const char* expression, const char* message, const char* file, i32 line) {
    E_FATAL("Assertion failure: %s, message: %s, in file: %s, line: %d", expression, message, file, line);
}
