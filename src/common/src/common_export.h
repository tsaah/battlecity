#pragma once

#if defined(COMMON_EXPORT)
    #ifdef _MSC_VER
        #define API_COMMON __declspec(dllexport)
    #else
        #define API_COMMON __attribute((visibility("default")))
    #endif
#else
    #ifdef _MSC_VER
        #define API_COMMON __declspec(dllimport)
    #else
        #define API_COMMON
    #endif
#endif
