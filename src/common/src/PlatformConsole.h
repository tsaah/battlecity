#pragma once
#include "basic_types.h"

namespace platform {

class PlatformConsole final {
public:
    void write(const char* message, u8 color) const;
    void writeError(const char* message, u8 color) const;
};

} // namespace platform
