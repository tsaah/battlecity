#pragma once

#include "common_export.h"

#include <basic_types.h>

API_COMMON u64 stringLength(const char* s);
// API_COMMON char* stringClone(const char* s);
API_COMMON bool stringEqual(const char* s1, const char* s2);