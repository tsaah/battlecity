#include "../PlatformConsole.h"
#include "../platform_detection.h"

#if EFREET_PLATFORM_WINDOWS

#include <Windows.h>

namespace platform {

void PlatformConsole::write(const char* message, u8 color) const {
    // E_ASSERT_EXT(color <= 6, "unexpected color value"); // TODO: assert here

    auto consoleHandle = ::GetStdHandle(STD_OUTPUT_HANDLE);
    static const u8 levels[6] = { 71, 116, 6, 2, 113, 8 };//{ 64, 4, 6, 2, 1, 8 };
    ::SetConsoleTextAttribute(consoleHandle, levels[color]);

    ::OutputDebugStringA(message);
    const auto length = static_cast<::DWORD>(::strlen(message));
    ::LPDWORD numberWritten = nullptr;
    ::WriteConsoleA(consoleHandle, message, length, numberWritten, nullptr);
}

void PlatformConsole::writeError(const char* message, u8 color) const {
    // E_ASSERT_EXT(color <= 6, "unexpected color value"); // TODO: assert here
    auto consoleHandle = ::GetStdHandle(STD_ERROR_HANDLE);
    static const u8 levels[6] = { 71, 116, 6, 2, 113, 8 };//{ 64, 4, 6, 2, 1, 8 };
    ::SetConsoleTextAttribute(consoleHandle, levels[color]);

    ::OutputDebugStringA(message);
    const auto length = static_cast<::DWORD>(::strlen(message));
    ::LPDWORD numberWritten = nullptr;
    ::WriteConsoleA(consoleHandle, message, length, numberWritten, nullptr);
}

} // namespace platform

#endif