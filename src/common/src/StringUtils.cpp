#include "StringUtils.h"

#include <string.h>

u64 stringLength(const char* s) {
    return ::strlen(s); // TODO: strlen
}

// char* stringClone(const char* s) {
//     const auto length = stringLength(s);
//     char* clone = E_MemoryManager.allocate<char, E_MemTag::STRING>(length + 1);
//     E_MemoryManager.copyMemory(s, clone, length + 1);
//     return clone;
// }

bool stringEqual(const char* s1, const char* s2) {
    return ::strcmp(s1, s2) == 0; // TODO: strcmp
}