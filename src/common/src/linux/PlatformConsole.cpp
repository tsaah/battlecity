#include "../PlatformConsole.h"
#include "../platform_detection.h"

#if EFREET_PLATFORM_LINUX

#include <stdio.h>

namespace platform {

void PlatformConsole::write(const char* message, u8 color) const {
    // E_ASSERT_EXT(color <= 6, "unexpected color value");
    const char* color_strings[] = {"0;41", "1;31", "1;33", "1;32", "1;34", "1;30"};
    printf("\033[%sm%s\033[0m", color_strings[color], message);
}

void PlatformConsole::writeError(const char* message, u8 color) const {
    // E_ASSERT_EXT(color <= 6, "unexpected color value");
    const char* color_strings[] = {"0;41", "1;31", "1;33", "1;32", "1;34", "1;30"};
    printf("\033[%sm%s\033[0m", color_strings[color], message);
}

} // namespace platform

#endif