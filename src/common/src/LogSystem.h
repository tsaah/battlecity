#pragma once
#include "common_export.h"
#include "basic_types.h"
#include "PlatformConsole.h"

#include <array>

#define LOG_WARN_ENABLED 1
#define LOG_INFO_ENABLED 1
#define LOG_DEBUG_ENABLED 1
#define LOG_TRACE_ENABLED 1
#define EFREET_ASSERTIONS_ENABLED


#if EFREET_RELEASE == 1
    #define LOG_DEBUG_ENABLED 0
    #define LOG_TRACE_ENABLED 0
#endif

class API_COMMON LogSystem final {
public:
    enum LogLevel {
        LVL_FATAL,
        LVL_ERROR,
        LVL_WARN,
        LVL_INFO,
        LVL_DEBUG,
        LVL_TRACE
    };
    static LogSystem& instance();
    void log(LogLevel level, const char* message, ...);
    void reportAssertionFailure(const char* expression, const char* message, const char* file, i32 line);

private:
    LogSystem() = default;
    LogSystem(const LogSystem&) = delete;
    LogSystem& operator=(const LogSystem&) = delete;

    LogLevel logLevel_{ LogLevel::LVL_WARN };
    platform::PlatformConsole console_;
    static const std::array<const char*, 6> LOG_LEVEL_STRINGS;

};


#define E_LogSystem ::LogSystem::instance()
#define E_LogLevel ::LogSystem::LogLevel

#define E_FATAL(message, ...) E_LogSystem.log(E_LogLevel::LVL_FATAL, message, ##__VA_ARGS__)

#ifndef E_ERROR
    #define E_ERROR(message, ...) E_LogSystem.log(E_LogLevel::LVL_ERROR, message, ##__VA_ARGS__)
#endif

#if LOG_WARN_ENABLED == 1
    #define E_WARN(message, ...) E_LogSystem.log(E_LogLevel::LVL_WARN, message, ##__VA_ARGS__)
#else
    #define E_WARN(message, ...)
#endif

#if LOG_INFO_ENABLED == 1
    #define E_INFO(message, ...) E_LogSystem.log(E_LogLevel::LVL_INFO, message, ##__VA_ARGS__)
#else
    #define E_INFO(message, ...)
#endif

#if LOG_DEBUG_ENABLED == 1
    #define E_DEBUG(message, ...) E_LogSystem.log(E_LogLevel::LVL_DEBUG, message, ##__VA_ARGS__)
#else
    #define E_DEBUG(message, ...)
#endif

#if LOG_TRACE_ENABLED == 1
    #define E_TRACE(message, ...) E_LogSystem.log(E_LogLevel::LVL_TRACE, message, ##__VA_ARGS__)
#else
    #define E_TRACE(message, ...)
#endif

#ifdef EFREET_ASSERTIONS_ENABLED
    #if _MSC_VER
        #include <intrin.h>
        #define debugBreak() __debugbreak()
    #else
        #define debugBreak() __builtin_trap()
    #endif

    #define E_ASSERT(expr) \
        { \
            if (expr) {} else { \
                E_LogSystem.reportAssertionFailure(#expr, "", __FILE__, __LINE__); \
                debugBreak(); \
            } \
        }

    #define E_ASSERT_EXT(expr, message) \
        { \
            if (expr) {} else { \
                E_LogSystem.reportAssertionFailure(#expr, message, __FILE__, __LINE__); \
                debugBreak(); \
            } \
        }

    #ifdef _DEBUG
        #define E_ASSERT_DEBUG(expr) \
        { \
            if (expr) {} else { \
                E_LogSystem.reportAssertionFailure(#expr, "", __FILE__, __LINE__); \
                debugBreak(); \
            } \
        }

        #define E_ASSERT_DEBUG_EXT(expr, message)  \
        { \
            if (expr) {} else { \
                E_LogSystem.reportAssertionFailure(#expr, message, __FILE__, __LINE__); \
                debugBreak(); \
            } \
        }

    #else
        #define E_ASSERT_DEBUG(expr)
        #define E_ASSERT_DEBUG_EXT(expr, message)
    #endif
#else
    #define E_ASSERT_EXT(expr, message)
    #define E_ASSERT(expr))
    #define E_ASSERT_DEBUG(expr)
    #define E_ASSERT_DEBUG_EXT(expr, message)
#endif

