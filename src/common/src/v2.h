#pragma once

#include "basic_types.h"

#include "MathUtils.h"
// #include <cmath>
// #include <algorithm>

inline bool fuzzyCompare(f64 p1, f64 p2)
{
    return (abs(p1 - p2) * 1000000000000. <= min(abs(p1), abs(p2)));
}

inline bool fuzzyCompare(f32 p1, f32 p2)
{
    return (abs(p1 - p2) * 100000.f <= min(abs(p1), abs(p2)));
}

inline bool fuzzyIsNull(f64 d)
{
    return abs(d) <= 0.000000000001;
}

inline bool fuzzyIsNull(f32 f)
{
    return abs(f) <= 0.00001f;
}

template<class T>
class Vector2d final {
public:
    union {
        struct {
            T x;
            T y;
        };
        struct {
            T width;
            T height;
        };
        T v[2] = { static_cast<T>(0), static_cast<T>(0) };
    };

    constexpr inline Vector2d() : x(static_cast<T>(0)), y(static_cast<T>(0)) { }
    constexpr inline Vector2d(T xpos, T ypos) : x(xpos), y(ypos) { }
    constexpr inline Vector2d(const Vector2d &p) : x(p.x), y(p.y) { }

    constexpr inline T manhattanLength() const { return abs(x) + abs(y); }
    constexpr Vector2d transposed() const noexcept { return { y, x }; }
    inline bool isNull() const;

    constexpr inline f32 length() const {
        // Need some extra precision if the length is very small.
        const auto len = static_cast<f64>(x) * static_cast<f64>(x) + static_cast<f64>(y) * static_cast<f64>(y);
        return static_cast<f32>(sqrt(len));
    }
    constexpr inline f32 lengthSquared() const {
        return x * x + y * y;
    }

    constexpr inline Vector2d normalized() const {
        // Need some extra precision if the length is very small.
        const f64 len = static_cast<f64>(x) * static_cast<f64>(x) + static_cast<f64>(y) * static_cast<f64>(y);
        if (FuzzyIsNull(len - 1.0f)) {
            return *this;
        } else if (!FuzzyIsNull(len)) {
            const f64 sqrtLen = sqrt(len);
            return Vector2d(static_cast<T>(static_cast<f64>(x) / sqrtLen), static_cast<T>(static_cast<f64>(y) / sqrtLen));
        } else {
            return Vector2d();
        }
    }
    constexpr inline void normalize() {
        // Need some extra precision if the length is very small.
        const f64 len = static_cast<f64>(x) * static_cast<f64>(x) + static_cast<f64>(y) * static_cast<f64>(y);
        if (fuzzyIsNull(len - 1.0f) || fuzzyIsNull(len)) {
            return;
        }

        const f64 sqrtLen = std::sqrt(len);

        x = static_cast<T>(static_cast<f64>(x) / sqrtLen);
        y = static_cast<T>(static_cast<f64>(y) / sqrtLen);
    }

    constexpr inline f32 distanceToPoint(const Vector2d &point) const {
        return (*this - point).length();
    }
    constexpr inline f32 distanceToLine(const Vector2d& point, const Vector2d& direction) const {
        if (direction.isNull()) {
            return (*this - point).length();
        }
        Vector2d p = point + dotProduct(*this - point, direction) * direction;
        return (*this - p).length();
    }

    constexpr static inline T dotProduct(const Vector2d &p1, const Vector2d &p2) { return p1.x * p2.x + p1.y * p2.y; }
    constexpr inline T dotProduct(const Vector2d &o) { return x * o.x + y * o.y; }

    constexpr inline Vector2d &operator+=(const Vector2d &p) { x += p.x; y += p.y; return *this; }
    constexpr inline Vector2d &operator-=(const Vector2d &p) { x -= p.x; y -= p.y; return *this; }
    constexpr inline Vector2d &operator*=(T c) { x *= c; y *= c; return *this; }
    constexpr inline Vector2d &operator/=(T c) { x /= c; y /= c; return *this; }

    friend constexpr inline bool fuzzyCompare(const Vector2d& v1, const Vector2d& v2);

    template<class K>
    constexpr inline Vector2d<K> typeCast() const {
        return Vector2d<K>(static_cast<K>(x), static_cast<K>(y));
    }
};

// _Pragma("clang diagnostic push") // TODO: compiler agnostic pragmas
// _Pragma("clang diagnostic ignored \"-Wfloat-equal\"")

template<class T>
inline bool Vector2d<T>::isNull() const { return x == static_cast<T>(0) && y == static_cast<T>(0); }

template<class T>
constexpr inline bool operator==(const Vector2d<T> &p1, const Vector2d<T> &p2) { return p1.x == p2.x && p1.y == p2.y; }

template<class T>
constexpr inline bool operator!=(const Vector2d<T> &p1, const Vector2d<T> &p2) { return p1.x != p2.x || p1.y != p2.y; }

// _Pragma("clang diagnostic pop")

template<class T>
constexpr inline const Vector2d<T> operator+(const Vector2d<T> &p1, const Vector2d<T> &p2)
{
    return Vector2d<T>(p1.x + p2.x, p1.y + p2.y);
}

template<class T>
constexpr inline const Vector2d<T> operator-(const Vector2d<T> &p1, const Vector2d<T> &p2)
{
    return Vector2d<T>(p1.x - p2.x, p1.y - p2.y);
}

template<class T>
constexpr inline const Vector2d<T> operator*(const Vector2d<T> &p, T c)
{
    return Vector2d<T>(p.x * c, p.y * c);
}

template<class T>
constexpr inline const Vector2d<T> operator*(T c, const Vector2d<T> &p)
{
    return Vector2d<T>(p.x * c, p.y * c);
}

template<class T>
constexpr inline const Vector2d<T> operator+(const Vector2d<T> &p)
{
    return p;
}

template<class T>
constexpr inline const Vector2d<T> operator-(const Vector2d<T> &p)
{
    return Vector2d<T>(-p.x, -p.y);
}

template<class T>
constexpr inline const Vector2d<T> operator/(const Vector2d<T> &p, T divisor)
{
    return Vector2d<T>(p.x / divisor, p.y / divisor);
}

using v2 = Vector2d<f32>;
using v2i = Vector2d<i32>;
using SizeF = v2;
using Size = v2i;


