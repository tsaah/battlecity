#pragma once

#include "basic_types.h"
#include "v2.h"

class Rect final {
public:
    constexpr Rect() noexcept : x1(0), y1(0), x2(-1), y2(-1) {}
    Rect constexpr RectFromTopLeftAndBottomRight(const v2i &topleft, const v2i &bottomright) noexcept;
    constexpr Rect(const v2i &topleft, const Size &size) noexcept;
    constexpr Rect(i32 left, i32 top, i32 width, i32 height) noexcept;

    constexpr inline bool isNull() const noexcept;
    constexpr inline bool isEmpty() const noexcept;
    constexpr inline bool isValid() const noexcept;

    constexpr inline i32 left() const noexcept;
    constexpr inline i32 top() const noexcept;
    constexpr inline i32 right() const noexcept;
    constexpr inline i32 bottom() const noexcept;
    Rect normalized() const noexcept;

    constexpr inline i32 x() const noexcept;
    constexpr inline i32 y() const noexcept;
    constexpr inline void setLeft(i32 pos) noexcept;
    constexpr inline void setTop(i32 pos) noexcept;
    constexpr inline void setRight(i32 pos) noexcept;
    constexpr inline void setBottom(i32 pos) noexcept;
    constexpr inline void setX(i32 x) noexcept;
    constexpr inline void setY(i32 y) noexcept;

    constexpr inline void setTopLeft(const v2i &p) noexcept;
    constexpr inline void setBottomRight(const v2i &p) noexcept;
    constexpr inline void setTopRight(const v2i &p) noexcept;
    constexpr inline void setBottomLeft(const v2i &p) noexcept;

    constexpr inline v2i topLeft() const noexcept;
    constexpr inline v2i bottomRight() const noexcept;
    constexpr inline v2i topRight() const noexcept;
    constexpr inline v2i bottomLeft() const noexcept;
    constexpr inline v2i center() const noexcept;

    constexpr inline void moveLeft(i32 pos) noexcept;
    constexpr inline void moveTop(i32 pos) noexcept;
    constexpr inline void moveRight(i32 pos) noexcept;
    constexpr inline void moveBottom(i32 pos) noexcept;
    constexpr inline void moveTopLeft(const v2i &p) noexcept;
    constexpr inline void moveBottomRight(const v2i &p) noexcept;
    constexpr inline void moveTopRight(const v2i &p) noexcept;
    constexpr inline void moveBottomLeft(const v2i &p) noexcept;
    constexpr inline void moveCenter(const v2i &p) noexcept;

    constexpr inline void translate(i32 dx, i32 dy) noexcept;
    constexpr inline void translate(const v2i &p) noexcept;
    constexpr inline Rect translated(i32 dx, i32 dy) const noexcept;
    constexpr inline Rect translated(const v2i &p) const noexcept;
    constexpr inline Rect transposed() const noexcept;

    constexpr inline void moveTo(i32 x, i32 t) noexcept;
    constexpr inline void moveTo(const v2i &p) noexcept;

    constexpr inline void setRect(i32 x, i32 y, i32 w, i32 h) noexcept;
    constexpr inline void getRect(i32 *x, i32 *y, i32 *w, i32 *h) const;

    constexpr inline void setCoords(i32 x1, i32 y1, i32 x2, i32 y2) noexcept;
    constexpr inline void getCoords(i32 *x1, i32 *y1, i32 *x2, i32 *y2) const;

    constexpr inline void adjust(i32 x1, i32 y1, i32 x2, i32 y2) noexcept;
    constexpr inline Rect adjusted(i32 x1, i32 y1, i32 x2, i32 y2) const noexcept;

    constexpr inline Size size() const noexcept;
    constexpr inline i32 width() const noexcept;
    constexpr inline i32 height() const noexcept;
    constexpr inline void setWidth(i32 w) noexcept;
    constexpr inline void setHeight(i32 h) noexcept;
    constexpr inline void setSize(const Size &s) noexcept;

    Rect operator|(const Rect &r) const noexcept;
    Rect operator&(const Rect &r) const noexcept;
    inline Rect& operator|=(const Rect &r) noexcept;
    inline Rect& operator&=(const Rect &r) noexcept;

    bool contains(const Rect &r, bool proper = false) const noexcept;
    bool contains(const v2i &p, bool proper=false) const noexcept;
    inline bool contains(i32 x, i32 y) const noexcept;
    inline bool contains(i32 x, i32 y, bool proper) const noexcept;
    inline Rect united(const Rect &other) const noexcept;
    inline Rect intersected(const Rect &other) const noexcept;
    bool intersects(const Rect &r) const noexcept;

    // constexpr inline Rect marginsAdded(const QMargins &margins) const noexcept; // TODO: consider adding those margins functions
    // constexpr inline Rect marginsRemoved(const QMargins &margins) const noexcept;
    // constexpr inline Rect &operator+=(const QMargins &margins) noexcept;
    // constexpr inline Rect &operator-=(const QMargins &margins) noexcept;

    friend constexpr inline bool operator==(const Rect &, const Rect &) noexcept;
    friend constexpr inline bool operator!=(const Rect &, const Rect &) noexcept;

    i32 x1;
    i32 y1;
    i32 x2;
    i32 y2;
};


constexpr inline bool operator==(const Rect &, const Rect &) noexcept;
constexpr inline bool operator!=(const Rect &, const Rect &) noexcept;

constexpr inline Rect::Rect(i32 aleft, i32 atop, i32 awidth, i32 aheight) noexcept
    : x1(aleft), y1(atop), x2(aleft + awidth - 1), y2(atop + aheight - 1) {}

Rect constexpr inline Rect::RectFromTopLeftAndBottomRight(const v2i &atopLeft, const v2i &abottomRight) noexcept {
    Rect a;
    a.x1 = atopLeft.x;
    a.y1 = atopLeft.y;
    a.x2 = abottomRight.x;
    a.y2 = abottomRight.y;
    return a;
}

constexpr inline Rect::Rect(const v2i &atopLeft, const Size &asize) noexcept
    : x1(atopLeft.x), y1(atopLeft.y), x2(atopLeft.x + asize.width - 1), y2(atopLeft.y + asize.height - 1) {}

constexpr inline bool Rect::isNull() const noexcept
{ return x2 == x1 - 1 && y2 == y1 - 1; }

constexpr inline bool Rect::isEmpty() const noexcept
{ return x1 > x2 || y1 > y2; }

constexpr inline bool Rect::isValid() const noexcept
{ return x1 <= x2 && y1 <= y2; }

constexpr inline i32 Rect::left() const noexcept
{ return x1; }

constexpr inline i32 Rect::top() const noexcept
{ return y1; }

constexpr inline i32 Rect::right() const noexcept
{ return x2; }

constexpr inline i32 Rect::bottom() const noexcept
{ return y2; }

constexpr inline i32 Rect::x() const noexcept
{ return x1; }

constexpr inline i32 Rect::y() const noexcept
{ return y1; }

constexpr inline void Rect::setLeft(i32 pos) noexcept
{ x1 = pos; }

constexpr inline void Rect::setTop(i32 pos) noexcept
{ y1 = pos; }

constexpr inline void Rect::setRight(i32 pos) noexcept
{ x2 = pos; }

constexpr inline void Rect::setBottom(i32 pos) noexcept
{ y2 = pos; }

constexpr inline void Rect::setTopLeft(const v2i &p) noexcept
{ x1 = p.x; y1 = p.y; }

constexpr inline void Rect::setBottomRight(const v2i &p) noexcept
{ x2 = p.x; y2 = p.y; }

constexpr inline void Rect::setTopRight(const v2i &p) noexcept
{ x2 = p.x; y1 = p.y; }

constexpr inline void Rect::setBottomLeft(const v2i &p) noexcept
{ x1 = p.x; y2 = p.y; }

constexpr inline void Rect::setX(i32 ax) noexcept
{ x1 = ax; }

constexpr inline void Rect::setY(i32 ay) noexcept
{ y1 = ay; }

constexpr inline v2i Rect::topLeft() const noexcept
{ return v2i(x1, y1); }

constexpr inline v2i Rect::bottomRight() const noexcept
{ return v2i(x2, y2); }

constexpr inline v2i Rect::topRight() const noexcept
{ return v2i(x2, y1); }

constexpr inline v2i Rect::bottomLeft() const noexcept
{ return v2i(x1, y2); }

constexpr inline v2i Rect::center() const noexcept
{ return v2i(static_cast<i32>((static_cast<i64>(x1) + x2) / 2), static_cast<i32>((static_cast<i64>(y1) + y2) / 2)); } // cast avoids overflow on addition

constexpr inline i32 Rect::width() const noexcept
{ return  x2 - x1 + 1; }

constexpr inline i32 Rect::height() const noexcept
{ return  y2 - y1 + 1; }

constexpr inline Size Rect::size() const noexcept
{ return Size(width(), height()); }

constexpr inline void Rect::translate(i32 dx, i32 dy) noexcept
{
    x1 += dx;
    y1 += dy;
    x2 += dx;
    y2 += dy;
}

constexpr inline void Rect::translate(const v2i &p) noexcept
{
    x1 += p.x;
    y1 += p.y;
    x2 += p.x;
    y2 += p.y;
}

constexpr inline Rect Rect::translated(i32 dx, i32 dy) const noexcept
{ return Rect(v2i(x1 + dx, y1 + dy), v2i(x2 + dx, y2 + dy)); }

constexpr inline Rect Rect::translated(const v2i &p) const noexcept
{ return Rect(v2i(x1 + p.x, y1 + p.y), v2i(x2 + p.x, y2 + p.y)); }

constexpr inline Rect Rect::transposed() const noexcept
{ return Rect(topLeft(), size().transposed()); }

constexpr inline void Rect::moveTo(i32 ax, i32 ay) noexcept
{
    x2 += ax - x1;
    y2 += ay - y1;
    x1 = ax;
    y1 = ay;
}

constexpr inline void Rect::moveTo(const v2i &p) noexcept
{
    x2 += p.x - x1;
    y2 += p.y - y1;
    x1 = p.x;
    y1 = p.y;
}

constexpr inline void Rect::moveLeft(i32 pos) noexcept
{ x2 += (pos - x1); x1 = pos; }

constexpr inline void Rect::moveTop(i32 pos) noexcept
{ y2 += (pos - y1); y1 = pos; }

constexpr inline void Rect::moveRight(i32 pos) noexcept
{
    x1 += (pos - x2);
    x2 = pos;
}

constexpr inline void Rect::moveBottom(i32 pos) noexcept
{
    y1 += (pos - y2);
    y2 = pos;
}

constexpr inline void Rect::moveTopLeft(const v2i &p) noexcept
{
    moveLeft(p.x);
    moveTop(p.y);
}

constexpr inline void Rect::moveBottomRight(const v2i &p) noexcept
{
    moveRight(p.x);
    moveBottom(p.y);
}

constexpr inline void Rect::moveTopRight(const v2i &p) noexcept
{
    moveRight(p.x);
    moveTop(p.y);
}

constexpr inline void Rect::moveBottomLeft(const v2i &p) noexcept
{
    moveLeft(p.x);
    moveBottom(p.y);
}

constexpr inline void Rect::moveCenter(const v2i &p) noexcept
{
    i32 w = x2 - x1;
    i32 h = y2 - y1;
    x1 = p.x - w / 2;
    y1 = p.y - h / 2;
    x2 = x1 + w;
    y2 = y1 + h;
}

constexpr inline void Rect::getRect(i32 *ax, i32 *ay, i32 *aw, i32 *ah) const
{
    *ax = x1;
    *ay = y1;
    *aw = x2 - x1 + 1;
    *ah = y2 - y1 + 1;
}

constexpr inline void Rect::setRect(i32 ax, i32 ay, i32 aw, i32 ah) noexcept
{
    x1 = ax;
    y1 = ay;
    x2 = (ax + aw - 1);
    y2 = (ay + ah - 1);
}

constexpr inline void Rect::getCoords(i32 *xp1, i32 *yp1, i32 *xp2, i32 *yp2) const
{
    *xp1 = x1;
    *yp1 = y1;
    *xp2 = x2;
    *yp2 = y2;
}

constexpr inline void Rect::setCoords(i32 xp1, i32 yp1, i32 xp2, i32 yp2) noexcept
{
    x1 = xp1;
    y1 = yp1;
    x2 = xp2;
    y2 = yp2;
}

constexpr inline Rect Rect::adjusted(i32 xp1, i32 yp1, i32 xp2, i32 yp2) const noexcept
{ return Rect(v2i(x1 + xp1, y1 + yp1), v2i(x2 + xp2, y2 + yp2)); }

constexpr inline void Rect::adjust(i32 dx1, i32 dy1, i32 dx2, i32 dy2) noexcept
{
    x1 += dx1;
    y1 += dy1;
    x2 += dx2;
    y2 += dy2;
}

constexpr inline void Rect::setWidth(i32 w) noexcept
{ x2 = (x1 + w - 1); }

constexpr inline void Rect::setHeight(i32 h) noexcept
{ y2 = (y1 + h - 1); }

constexpr inline void Rect::setSize(const Size &s) noexcept
{
    x2 = (s.width  + x1 - 1);
    y2 = (s.height + y1 - 1);
}

inline bool Rect::contains(i32 ax, i32 ay, bool aproper) const noexcept
{
    return contains(v2i(ax, ay), aproper);
}

inline bool Rect::contains(i32 ax, i32 ay) const noexcept
{
    return contains(v2i(ax, ay), false);
}

inline Rect& Rect::operator|=(const Rect &r) noexcept
{
    *this = *this | r;
    return *this;
}

inline Rect& Rect::operator&=(const Rect &r) noexcept
{
    *this = *this & r;
    return *this;
}

inline Rect Rect::intersected(const Rect &other) const noexcept
{
    return *this & other;
}

inline Rect Rect::united(const Rect &r) const noexcept
{
    return *this | r;
}

constexpr inline bool operator==(const Rect &r1, const Rect &r2) noexcept
{
    return r1.x1==r2.x1 && r1.x2==r2.x2 && r1.y1==r2.y1 && r1.y2==r2.y2;
}

constexpr inline bool operator!=(const Rect &r1, const Rect &r2) noexcept
{
    return r1.x1!=r2.x1 || r1.x2!=r2.x2 || r1.y1!=r2.y1 || r1.y2!=r2.y2;
}

// constexpr inline Rect operator+(const Rect &rectangle, const QMargins &margins) noexcept
// {
//     return Rect(v2i(rectangle.left() - margins.left(), rectangle.top() - margins.top()),
//                  v2i(rectangle.right() + margins.right(), rectangle.bottom() + margins.bottom()));
// }

// constexpr inline Rect operator+(const QMargins &margins, const Rect &rectangle) noexcept
// {
//     return Rect(v2i(rectangle.left() - margins.left(), rectangle.top() - margins.top()),
//                  v2i(rectangle.right() + margins.right(), rectangle.bottom() + margins.bottom()));
// }

// constexpr inline Rect operator-(const Rect &lhs, const QMargins &rhs) noexcept
// {
//     return Rect(v2i(lhs.left() + rhs.left(), lhs.top() + rhs.top()),
//                  v2i(lhs.right() - rhs.right(), lhs.bottom() - rhs.bottom()));
// }

// constexpr inline Rect Rect::marginsAdded(const QMargins &margins) const noexcept
// {
//     return Rect(v2i(x1 - margins.left(), y1 - margins.top()),
//                  v2i(x2 + margins.right(), y2 + margins.bottom()));
// }

// constexpr inline Rect Rect::marginsRemoved(const QMargins &margins) const noexcept
// {
//     return Rect(v2i(x1 + margins.left(), y1 + margins.top()),
//                  v2i(x2 - margins.right(), y2 - margins.bottom()));
// }

// constexpr inline Rect &Rect::operator+=(const QMargins &margins) noexcept
// {
//     *this = marginsAdded(margins);
//     return *this;
// }

// constexpr inline Rect &Rect::operator-=(const QMargins &margins) noexcept
// {
//     *this = marginsRemoved(margins);
//     return *this;
// }


class RectF final {
public:
    f32 xp;
    f32 yp;
    f32 w;
    f32 h;

    constexpr RectF() noexcept : xp(0.), yp(0.), w(0.), h(0.) {}
    constexpr RectF(const v2 &topleft, const SizeF &size) noexcept;
    RectF constexpr RectFFromTopLeftAndBottomRight(const v2 &topleft, const v2 &bottomRight) noexcept;
    constexpr RectF(f32 left, f32 top, f32 width, f32 height) noexcept;
    constexpr RectF(const Rect &rect) noexcept;

    constexpr inline bool isNull() const noexcept;
    constexpr inline bool isEmpty() const noexcept;
    constexpr inline bool isValid() const noexcept;
    RectF normalized() const noexcept;

    constexpr inline f32 left() const noexcept { return xp; }
    constexpr inline f32 top() const noexcept { return yp; }
    constexpr inline f32 right() const noexcept { return xp + w; }
    constexpr inline f32 bottom() const noexcept { return yp + h; }

    constexpr inline f32 x() const noexcept;
    constexpr inline f32 y() const noexcept;
    constexpr inline void setLeft(f32 pos) noexcept;
    constexpr inline void setTop(f32 pos) noexcept;
    constexpr inline void setRight(f32 pos) noexcept;
    constexpr inline void setBottom(f32 pos) noexcept;
    constexpr inline void setX(f32 pos) noexcept { setLeft(pos); }
    constexpr inline void setY(f32 pos) noexcept { setTop(pos); }

    constexpr inline v2 topLeft() const noexcept { return v2(xp, yp); }
    constexpr inline v2 bottomRight() const noexcept { return v2(xp+w, yp+h); }
    constexpr inline v2 topRight() const noexcept { return v2(xp+w, yp); }
    constexpr inline v2 bottomLeft() const noexcept { return v2(xp, yp+h); }
    constexpr inline v2 center() const noexcept;

    constexpr inline void setTopLeft(const v2 &p) noexcept;
    constexpr inline void setBottomRight(const v2 &p) noexcept;
    constexpr inline void setTopRight(const v2 &p) noexcept;
    constexpr inline void setBottomLeft(const v2 &p) noexcept;

    constexpr inline void moveLeft(f32 pos) noexcept;
    constexpr inline void moveTop(f32 pos) noexcept;
    constexpr inline void moveRight(f32 pos) noexcept;
    constexpr inline void moveBottom(f32 pos) noexcept;
    constexpr inline void moveTopLeft(const v2 &p) noexcept;
    constexpr inline void moveBottomRight(const v2 &p) noexcept;
    constexpr inline void moveTopRight(const v2 &p) noexcept;
    constexpr inline void moveBottomLeft(const v2 &p) noexcept;
    constexpr inline void moveCenter(const v2 &p) noexcept;

    constexpr inline void translate(f32 dx, f32 dy) noexcept;
    constexpr inline void translate(const v2 &p) noexcept;

    constexpr inline RectF translated(f32 dx, f32 dy) const noexcept;
    constexpr inline RectF translated(const v2 &p) const noexcept;

    constexpr inline RectF transposed() const noexcept;

    constexpr inline void moveTo(f32 x, f32 y) noexcept;
    constexpr inline void moveTo(const v2 &p) noexcept;

    constexpr inline void setRect(f32 x, f32 y, f32 w, f32 h) noexcept;
    constexpr inline void getRect(f32 *x, f32 *y, f32 *w, f32 *h) const;

    constexpr inline void setCoords(f32 x1, f32 y1, f32 x2, f32 y2) noexcept;
    constexpr inline void getCoords(f32 *x1, f32 *y1, f32 *x2, f32 *y2) const;

    constexpr inline void adjust(f32 x1, f32 y1, f32 x2, f32 y2) noexcept;
    constexpr inline RectF adjusted(f32 x1, f32 y1, f32 x2, f32 y2) const noexcept;

    constexpr inline SizeF size() const noexcept;
    constexpr inline f32 width() const noexcept;
    constexpr inline f32 height() const noexcept;
    constexpr inline void setWidth(f32 w) noexcept;
    constexpr inline void setHeight(f32 h) noexcept;
    constexpr inline void setSize(const SizeF &s) noexcept;

    RectF operator|(const RectF &r) const noexcept;
    RectF operator&(const RectF &r) const noexcept;
    inline RectF& operator|=(const RectF &r) noexcept;
    inline RectF& operator&=(const RectF &r) noexcept;

    bool contains(const RectF &r) const noexcept;
    bool contains(const v2 &p) const noexcept;
    inline bool contains(f32 x, f32 y) const noexcept;
    inline RectF united(const RectF &other) const noexcept;
    inline RectF intersected(const RectF &other) const noexcept;
    bool intersects(const RectF &r) const noexcept;

    // constexpr inline RectF marginsAdded(const QMarginsF &margins) const noexcept;
    // constexpr inline RectF marginsRemoved(const QMarginsF &margins) const noexcept;
    // constexpr inline RectF &operator+=(const QMarginsF &margins) noexcept;
    // constexpr inline RectF &operator-=(const QMarginsF &margins) noexcept;

    constexpr inline Rect toRect() const noexcept;
    Rect toAlignedRect() const noexcept;


};

inline bool operator==(const RectF &, const RectF &) noexcept; // TODO: make constexpr after making abs
inline bool operator!=(const RectF &, const RectF &) noexcept; // TODO: make constexpr after making abs

constexpr inline RectF::RectF(f32 aleft, f32 atop, f32 awidth, f32 aheight) noexcept
    : xp(aleft), yp(atop), w(awidth), h(aheight)
{
}

constexpr inline RectF::RectF(const v2 &atopLeft, const SizeF &asize) noexcept
    : xp(atopLeft.x), yp(atopLeft.y), w(asize.width), h(asize.height)
{
}


RectF constexpr inline RectF::RectFFromTopLeftAndBottomRight(const v2 &atopLeft, const v2 &abottomRight) noexcept
{
    RectF a;
    a.xp = atopLeft.x;
    a.yp = atopLeft.y;
    a.w = abottomRight.x - atopLeft.x;
    a.h = abottomRight.y - atopLeft.y;
    return a;
}

constexpr inline RectF::RectF(const Rect &r) noexcept
    : xp(r.x()), yp(r.y()), w(r.width()), h(r.height())
{
}

// QT_WARNING_PUSH // TODO: float comparison
// QT_WARNING_DISABLE_CLANG("-Wfloat-equal")
// QT_WARNING_DISABLE_GCC("-Wfloat-equal")
// QT_WARNING_DISABLE_INTEL(1572)

constexpr inline bool RectF::isNull() const noexcept
{ return w == 0. && h == 0.; }

constexpr inline bool RectF::isEmpty() const noexcept
{ return w <= 0. || h <= 0.; }

// QT_WARNING_POP

constexpr inline bool RectF::isValid() const noexcept
{ return w > 0. && h > 0.; }

constexpr inline f32 RectF::x() const noexcept
{ return xp; }

constexpr inline f32 RectF::y() const noexcept
{ return yp; }

constexpr inline void RectF::setLeft(f32 pos) noexcept
{ f32 diff = pos - xp; xp += diff; w -= diff; }

constexpr inline void RectF::setRight(f32 pos) noexcept
{ w = pos - xp; }

constexpr inline void RectF::setTop(f32 pos) noexcept
{ f32 diff = pos - yp; yp += diff; h -= diff; }

constexpr inline void RectF::setBottom(f32 pos) noexcept
{ h = pos - yp; }

constexpr inline void RectF::setTopLeft(const v2 &p) noexcept
{ setLeft(p.x); setTop(p.y); }

constexpr inline void RectF::setTopRight(const v2 &p) noexcept
{ setRight(p.x); setTop(p.y); }

constexpr inline void RectF::setBottomLeft(const v2 &p) noexcept
{ setLeft(p.x); setBottom(p.y); }

constexpr inline void RectF::setBottomRight(const v2 &p) noexcept
{ setRight(p.x); setBottom(p.y); }

constexpr inline v2 RectF::center() const noexcept
{ return v2(xp + w/2, yp + h/2); }

constexpr inline void RectF::moveLeft(f32 pos) noexcept
{ xp = pos; }

constexpr inline void RectF::moveTop(f32 pos) noexcept
{ yp = pos; }

constexpr inline void RectF::moveRight(f32 pos) noexcept
{ xp = pos - w; }

constexpr inline void RectF::moveBottom(f32 pos) noexcept
{ yp = pos - h; }

constexpr inline void RectF::moveTopLeft(const v2 &p) noexcept
{ moveLeft(p.x); moveTop(p.y); }

constexpr inline void RectF::moveTopRight(const v2 &p) noexcept
{ moveRight(p.x); moveTop(p.y); }

constexpr inline void RectF::moveBottomLeft(const v2 &p) noexcept
{ moveLeft(p.x); moveBottom(p.y); }

constexpr inline void RectF::moveBottomRight(const v2 &p) noexcept
{ moveRight(p.x); moveBottom(p.y); }

constexpr inline void RectF::moveCenter(const v2 &p) noexcept
{ xp = p.x - w / 2; yp = p.y - h / 2; }

constexpr inline f32 RectF::width() const noexcept
{ return w; }

constexpr inline f32 RectF::height() const noexcept
{ return h; }

constexpr inline SizeF RectF::size() const noexcept
{ return SizeF(w, h); }

constexpr inline void RectF::translate(f32 dx, f32 dy) noexcept
{
    xp += dx;
    yp += dy;
}

constexpr inline void RectF::translate(const v2 &p) noexcept
{
    xp += p.x;
    yp += p.y;
}

constexpr inline void RectF::moveTo(f32 ax, f32 ay) noexcept
{
    xp = ax;
    yp = ay;
}

constexpr inline void RectF::moveTo(const v2 &p) noexcept
{
    xp = p.x;
    yp = p.y;
}

constexpr inline RectF RectF::translated(f32 dx, f32 dy) const noexcept
{ return RectF(xp + dx, yp + dy, w, h); }

constexpr inline RectF RectF::translated(const v2 &p) const noexcept
{ return RectF(xp + p.x, yp + p.y, w, h); }

constexpr inline RectF RectF::transposed() const noexcept
{ return RectF(topLeft(), size().transposed()); }

constexpr inline void RectF::getRect(f32 *ax, f32 *ay, f32 *aaw, f32 *aah) const
{
    *ax = this->xp;
    *ay = this->yp;
    *aaw = this->w;
    *aah = this->h;
}

constexpr inline void RectF::setRect(f32 ax, f32 ay, f32 aaw, f32 aah) noexcept
{
    this->xp = ax;
    this->yp = ay;
    this->w = aaw;
    this->h = aah;
}

constexpr inline void RectF::getCoords(f32 *xp1, f32 *yp1, f32 *xp2, f32 *yp2) const
{
    *xp1 = xp;
    *yp1 = yp;
    *xp2 = xp + w;
    *yp2 = yp + h;
}

constexpr inline void RectF::setCoords(f32 xp1, f32 yp1, f32 xp2, f32 yp2) noexcept
{
    xp = xp1;
    yp = yp1;
    w = xp2 - xp1;
    h = yp2 - yp1;
}

constexpr inline void RectF::adjust(f32 xp1, f32 yp1, f32 xp2, f32 yp2) noexcept
{ xp += xp1; yp += yp1; w += xp2 - xp1; h += yp2 - yp1; }

constexpr inline RectF RectF::adjusted(f32 xp1, f32 yp1, f32 xp2, f32 yp2) const noexcept
{ return RectF(xp + xp1, yp + yp1, w + xp2 - xp1, h + yp2 - yp1); }

constexpr inline void RectF::setWidth(f32 aw) noexcept
{ this->w = aw; }

constexpr inline void RectF::setHeight(f32 ah) noexcept
{ this->h = ah; }

constexpr inline void RectF::setSize(const SizeF &s) noexcept
{
    w = s.width;
    h = s.height;
}

inline bool RectF::contains(f32 ax, f32 ay) const noexcept
{
    return contains(v2(ax, ay));
}

inline RectF& RectF::operator|=(const RectF &r) noexcept
{
    *this = *this | r;
    return *this;
}

inline RectF& RectF::operator&=(const RectF &r) noexcept
{
    *this = *this & r;
    return *this;
}

inline RectF RectF::intersected(const RectF &r) const noexcept
{
    return *this & r;
}

inline RectF RectF::united(const RectF &r) const noexcept
{
    return *this | r;
}

inline bool operator==(const RectF &r1, const RectF &r2) noexcept // TODO: make constexpr after making abs
{
    return fuzzyCompare(r1.xp, r2.xp) && fuzzyCompare(r1.yp, r2.yp)
           && fuzzyCompare(r1.w, r2.w) && fuzzyCompare(r1.h, r2.h);
}

inline bool operator!=(const RectF &r1, const RectF &r2) noexcept // TODO: make constexpr after making abs
{
    return !fuzzyCompare(r1.xp, r2.xp) || !fuzzyCompare(r1.yp, r2.yp)
           || !fuzzyCompare(r1.w, r2.w) || !fuzzyCompare(r1.h, r2.h);
}

constexpr inline Rect RectF::toRect() const noexcept
{
    return Rect(v2i(std::roundf(xp), std::roundf(yp)), v2i(std::roundf(xp + w) - 1, std::roundf(yp + h) - 1)); // TODO: std::roundf
}

// constexpr inline RectF operator+(const RectF &lhs, const QMarginsF &rhs) noexcept
// {
//     return RectF(v2(lhs.left() - rhs.left(), lhs.top() - rhs.top()),
//                   SizeF(lhs.width() + rhs.left() + rhs.right(), lhs.height() + rhs.top() + rhs.bottom()));
// }

// constexpr inline RectF operator+(const QMarginsF &lhs, const RectF &rhs) noexcept
// {
//     return RectF(v2(rhs.left() - lhs.left(), rhs.top() - lhs.top()),
//                   SizeF(rhs.width() + lhs.left() + lhs.right(), rhs.height() + lhs.top() + lhs.bottom()));
// }

// constexpr inline RectF operator-(const RectF &lhs, const QMarginsF &rhs) noexcept
// {
//     return RectF(v2(lhs.left() + rhs.left(), lhs.top() + rhs.top()),
//                   SizeF(lhs.width() - rhs.left() - rhs.right(), lhs.height() - rhs.top() - rhs.bottom()));
// }

// constexpr inline RectF RectF::marginsAdded(const QMarginsF &margins) const noexcept
// {
//     return RectF(v2(xp - margins.left(), yp - margins.top()),
//                   SizeF(w + margins.left() + margins.right(), h + margins.top() + margins.bottom()));
// }

// constexpr inline RectF RectF::marginsRemoved(const QMarginsF &margins) const noexcept
// {
//     return RectF(v2(xp + margins.left(), yp + margins.top()),
//                   SizeF(w - margins.left() - margins.right(), h - margins.top() - margins.bottom()));
// }

// constexpr inline RectF &RectF::operator+=(const QMarginsF &margins) noexcept
// {
//     *this = marginsAdded(margins);
//     return *this;
// }

// constexpr inline RectF &RectF::operator-=(const QMarginsF &margins) noexcept
// {
//     *this = marginsRemoved(margins);
//     return *this;
// }
