#pragma once

#include "basic_types.h"

#include <cmath> // TODO: remove

template<class T>
inline constexpr T clamp(T value, T min, T max) {
    return (value <= min) ? min : ((value >= max) ? max : value);
}

template<class T>
inline constexpr T min(T a, T b) {
    return (a <= b) ? a : b;
}

template<class T>
inline constexpr T max(T a, T b) {
    return (a >= b) ? a : b;
}

template<class T>
inline constexpr T abs(T a) {
    return (a < 0) ? -a : a;
}


template<class T>
inline constexpr T sqrt(T a) {
    return std::sqrt(a); // TODO: sqrt
}