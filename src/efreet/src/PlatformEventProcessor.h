#pragma once

class Application;

class PlatformEventProcessor final {
public:
    bool processEvents(Application& application);
};