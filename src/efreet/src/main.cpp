#include "Application.h"

#include <LogSystem.h>

int main(int argc, char** argv) {
    Application application;

    if (!application.init(argc, argv)) {
        E_FATAL("Failed to initialize application");
        return -1;
    }

    if (!application.exec()) {
        E_FATAL("Failed to execute application");
        return -2;
    }

    return 0;
}