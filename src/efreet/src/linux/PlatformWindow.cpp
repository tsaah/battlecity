#include "../PlatformWindow.h"
#if EFREET_PLATFORM_LINUX

#include <core/LogSystem.h>

#include <xcb/xcb.h>
#include <X11/keysym.h>
#include <X11/XKBlib.h>  // sudo apt-get install libx11-dev
#include <X11/Xlib.h>
#include <X11/Xlib-xcb.h>  // sudo apt-get install libxkbcommon-x11-dev

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

namespace efreet::platform {

PlatformWindow::~PlatformWindow() {
    XAutoRepeatOn(state_.display);
    xcb_destroy_window(state_.connection, state_.window);
}

bool PlatformWindow::create(const Config& config) {
    state_.display = XOpenDisplay(nullptr);
    E_ASSERT(state_.display);

    XAutoRepeatOff(state_.display);

    state_.connection = XGetXCBConnection(state_.display);
    E_ASSERT(state_.connection);

    const auto* setup = xcb_get_setup(state_.connection);
    E_ASSERT(setup);

    auto it = xcb_setup_roots_iterator(setup);
    int screen = 0; // TODO: understand what happens here
    for (i32 s = screen; s > 0; --s) {
        xcb_screen_next(&it);
    }

    state_.screen = it.data;
    E_ASSERT(state_.screen);

    state_.window = xcb_generate_id(state_.connection);

    u32 eventMask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
    u32 eventValues = XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE |
        XCB_EVENT_MASK_KEY_PRESS | XCB_EVENT_MASK_KEY_RELEASE |
        XCB_EVENT_MASK_EXPOSURE | XCB_EVENT_MASK_POINTER_MOTION |
        XCB_EVENT_MASK_STRUCTURE_NOTIFY;

    u32 valueList[] = { state_.screen->black_pixel, eventValues };

    auto cookie = xcb_create_window(
        state_.connection,
        XCB_COPY_FROM_PARENT,
        state_.window,
        state_.screen->root,
        config.x, config.y, config.width, config.height,
        0,
        XCB_WINDOW_CLASS_INPUT_OUTPUT,
        state_.screen->root_visual,
        eventMask,
        valueList
    );

    xcb_change_property(
        state_.connection,
        XCB_PROP_MODE_REPLACE,
        state_.window,
        XCB_ATOM_WM_NAME,
        XCB_ATOM_STRING,
        8,
        strlen(config.title), // TODO: strlen
        config.title
    );

    auto wm_delete_cookie = xcb_intern_atom(
        state_.connection,
        0,
        strlen("WM_DELETE_WINDOW"),
        "WM_DELETE_WINDOW");
    auto wm_protocols_cookie = xcb_intern_atom(
        state_.connection,
        0,
        strlen("WM_PROTOCOLS"),
        "WM_PROTOCOLS");
    auto* wm_delete_reply = xcb_intern_atom_reply(
        state_.connection,
        wm_delete_cookie,
        nullptr);
    auto* wm_protocols_reply = xcb_intern_atom_reply(
        state_.connection,
        wm_protocols_cookie,
        nullptr);
    state_.wm_delete_win = wm_delete_reply->atom;
    state_.wm_protocols = wm_protocols_reply->atom;

    xcb_change_property(
        state_.connection,
        XCB_PROP_MODE_REPLACE,
        state_.window,
        wm_protocols_reply->atom,
        4,
        32,
        1,
        &wm_delete_reply->atom);

    // Map the window to the screen
    xcb_map_window(state_.connection, state_.window);

    // Flush the stream
    i32 stream_result = xcb_flush(state_.connection);
    if (stream_result <= 0) {
        E_FATAL("An error occurred when flusing the stream: %d", stream_result);
        return false;
    }

    return true;
}

} // namespace efreet::platform

#endif