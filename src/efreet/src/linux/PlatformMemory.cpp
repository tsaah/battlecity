#include "../PlatformMemory.h"

#include <platform_detection.h>

#if EFREET_PLATFORM_LINUX

#include <stdlib.h>
#include <cstring>

void PlatformMemory::free(void* address, bool aligned) const {
	std::free(address);
}

void* PlatformMemory::alloc(size_t size, bool aligned) const {
	return std::malloc(size);
}

void* PlatformMemory::zero(void* address, size_t size) const {
	return std::memset(address, 0, size);
}

void* PlatformMemory::copy(const void* source, size_t size, void* destination) const {
	return std::memcpy(destination, source, size);
}

#endif