#pragma once

#include <platform_detection.h>

#if EFREET_PLATFORM_LINUX

#include <xcb/xcb.h>
#include <X11/keysym.h>
#include <X11/XKBlib.h>  // sudo apt-get install libx11-dev
#include <X11/Xlib.h>
#include <X11/Xlib-xcb.h>  // sudo apt-get install libxkbcommon-x11-dev

namespace efreet::platform {

struct PlatformWindowState final {
    Display* display{ nullptr };
    xcb_connection_t* connection{ nullptr };
    xcb_window_t window{ 0 };
    xcb_screen_t* screen{ nullptr };
    xcb_atom_t wm_protocols{ 0 };
    xcb_atom_t wm_delete_win{ 0 };
};

} // namespace efreet::platform

#endif