#pragma once
#include <basic_types.h>
#include <rect.h>

#include "PlatformWindowState.h"
class PlatformWindow final {
public:
	struct Config {
		const char* title{ nullptr };
		Rect rect;
	};
	~PlatformWindow();

    bool create(const Config& config);
	void close();
	inline const PlatformWindowState& state() const { return state_; }
	bool isOpen() const;

private:
	PlatformWindowState state_;
	bool isOpen_{ false };
};
