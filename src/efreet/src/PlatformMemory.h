#pragma once

class PlatformMemory final {
public:
    void free(void* address, bool aligned) const;
    void* alloc(size_t size, bool aligned) const;
    void* zero(void* address, size_t size) const;
    void* copy(const void* source, size_t size, void* destination) const;
};
