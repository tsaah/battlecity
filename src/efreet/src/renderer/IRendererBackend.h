#pragma once

#include <v2.h>

#include <memory>

struct PlatformWindowState;

class IRendererBackend {
public:
    virtual ~IRendererBackend() = default;

    virtual bool init(const char* applicationName, const PlatformWindowState& windowState) = 0;
    virtual void resize(Size size) = 0;
    virtual bool beginFrame(f32 dt) = 0;
    virtual bool endFrame(f32 dt) = 0;
};

using IRendererBackendPtr = std::unique_ptr<IRendererBackend>;