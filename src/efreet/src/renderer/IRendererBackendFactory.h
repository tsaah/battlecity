#pragma once

#include "RendererBackendType.h"

#include <memory>

class IRendererBackend;

class IRendererBackendFactory {
public:
    virtual ~IRendererBackendFactory() = default;

    virtual std::unique_ptr<IRendererBackend> create(RendererBackendType type) = 0;
};

using IRendererBackendFactoryPtr = std::unique_ptr<IRendererBackendFactory>;