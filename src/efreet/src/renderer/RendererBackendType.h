#pragma once

enum class RendererBackendType {
    VULKAN,
    OPENGL,
    DIRECTX
};