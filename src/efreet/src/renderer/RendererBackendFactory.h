#pragma once

#include "IRendererBackendFactory.h"

class RendererBackendFactory final: public IRendererBackendFactory {
public:
    ~RendererBackendFactory() override = default;
    std::unique_ptr<IRendererBackend> create(RendererBackendType type) override;
};