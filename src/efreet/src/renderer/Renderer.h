#pragma once

#include "IRendererBackend.h"
#include "IRendererBackendFactory.h"
#include "RenderPacket.h"

#include <basic_types.h>
#include <v2.h>

struct PlatformWindowState;

class Renderer final {
public:
    bool init(const char* applicationName, const PlatformWindowState& windowState, IRendererBackendFactoryPtr&& backendFactory);

    void onResized(Size size);
    bool drawFrame(const RenderPacketPtr& packet);

private:
    IRendererBackendPtr backend_;
    IRendererBackendFactoryPtr rendererBackendFactory_;
};