#pragma once

#include <basic_types.h>

#include <memory>

struct RenderPacket {
    f32 dt_{ 0.0f };
};

using RenderPacketPtr = std::unique_ptr<RenderPacket>;