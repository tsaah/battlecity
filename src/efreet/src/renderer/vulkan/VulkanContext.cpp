#include "VulkanContext.h"

VulkanContext::SwapchainSupportInfo VulkanContext::querySwapchainSupport(VkPhysicalDevice aPhysicalDevice, VkSurfaceKHR aSurface) {
    VulkanContext::SwapchainSupportInfo info;
    E_ASSERT(aPhysicalDevice);
    E_ASSERT(aSurface);

    VK_CHECK(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(aPhysicalDevice, aSurface, &info.capabilities));

    u32 formatCount = 0;
    VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(aPhysicalDevice, aSurface, &formatCount, nullptr));
    if (formatCount > 0) {
        info.formats.resize(formatCount);
        VK_CHECK(vkGetPhysicalDeviceSurfaceFormatsKHR(aPhysicalDevice, aSurface, &formatCount, info.formats.data()));
    }

    u32 presentModeCount = 0;
    VK_CHECK(vkGetPhysicalDeviceSurfacePresentModesKHR(aPhysicalDevice, aSurface, &presentModeCount, nullptr));
    if (presentModeCount > 0) {
        info.presentModes.resize(presentModeCount);
        VK_CHECK(vkGetPhysicalDeviceSurfacePresentModesKHR(aPhysicalDevice, aSurface, &presentModeCount, info.presentModes.data()));
    }
    return info;
}


VkFormat VulkanContext::detectDepthBuffer(VkPhysicalDevice aPhysicalDevice) {
    auto result = VK_FORMAT_UNDEFINED;
    const std::array<VkFormat, 3> candidates = {
        VK_FORMAT_D32_SFLOAT,
        VK_FORMAT_D32_SFLOAT_S8_UINT,
        VK_FORMAT_D24_UNORM_S8_UINT
    };
    const u32 flags = VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT;
    for (const auto& c: candidates) {
        VkFormatProperties properties;
        vkGetPhysicalDeviceFormatProperties(aPhysicalDevice, c, &properties);

        if ((properties.linearTilingFeatures & flags) == flags || (properties.optimalTilingFeatures & flags) == flags) {
            result = c;
        }
    }
    return result;
}

i32 VulkanContext::findMemoryIndex(VkPhysicalDevice aPhysicalDevice, u32 typeFilteer, u32 propertyFlags) {
    VkPhysicalDeviceMemoryProperties memoryProperties;
    vkGetPhysicalDeviceMemoryProperties(aPhysicalDevice, &memoryProperties);

    for (auto i = 0; i < memoryProperties.memoryTypeCount; ++i) {
        if (typeFilteer & (1 << i) && (memoryProperties.memoryTypes[i].propertyFlags & propertyFlags) == propertyFlags) {
            return i;
        }
    }

    E_WARN("unable to find suitable memory type");
    return -1;
}