#pragma once

#include <basic_types.h>
#include <LogSystem.h>

#include <vulkan/vulkan.h>

#define VK_CHECK(e) \
    { \
        E_ASSERT((e) == VK_SUCCESS); \
    }