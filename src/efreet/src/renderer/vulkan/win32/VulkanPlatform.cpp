#include <platform_detection.h>

#if EFREET_PLATFORM_WINDOWS

#include "../VulkanPlatform.h"
#include "../VulkanContext.h"
#include <PlatformWindowState.h>

#include <Windows.h>
#include <vulkan/vulkan_win32.h>

std::vector<const char*> VulkanPlatform::getRequiredVulkanExtensionNames() {
    std::vector<const char*> result;
    result.push_back("VK_KHR_win32_surface");
    return result;
}

VkSurfaceKHR VulkanPlatform::createVulkanSurface(const PlatformWindowState& windowState, const VulkanContext& context) {

    VkWin32SurfaceCreateInfoKHR createSurfaceInfo = { VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR };
    createSurfaceInfo.hinstance = windowState.hInstance_;
    createSurfaceInfo.hwnd = windowState.hWnd_;

    VkSurfaceKHR surface;
    VK_CHECK(vkCreateWin32SurfaceKHR(context.instance, &createSurfaceInfo, context.allocator, &surface));

    return surface;
}

#endif