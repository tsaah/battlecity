#include "VulkanDevice.h"

#include <StringUtils.h>

bool VulkanDevice::init(VulkanContext& context) {
    if (!selectPhysicalDevice(context)) {
        return false;
    }

    bool presentSharesGraphicsQueue = context.device.graphicsQueueindex == context.device.presentQueueIndex;
    bool transferSharesGraphicsQueue = context.device.graphicsQueueindex == context.device.transferQueueIndex;
    u32 indexCount = 1;
    if (!presentSharesGraphicsQueue) {
        ++indexCount;
    }
    if (!transferSharesGraphicsQueue) {
        ++indexCount;
    }
    std::vector<u32> indices;
    indices.resize(indexCount);
    u32 index = 0;
    indices[index++] = context.device.graphicsQueueindex;
    if (!presentSharesGraphicsQueue) {
        indices[index++] = context.device.presentQueueIndex;
    }
    if (!transferSharesGraphicsQueue) {
        indices[index++] = context.device.transferQueueIndex;
    }

    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    queueCreateInfos.reserve(indexCount);
    for (auto i = 0u; i < indexCount; ++i) {
        VkDeviceQueueCreateInfo info = { VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO };
        info.queueFamilyIndex = indices.at(i);
        info.queueCount = 1;
        // if (indices.at(i) == graphicsQueueindex_) {
        //     info.queueCount = 2;
        // }
        const f32 queuePriority = 1.0f;
        info.pQueuePriorities = &queuePriority;
        // f32 queuePriorities[] = { 1.0f, 1.0f };
        // info.pQueuePriorities = queuePriorities;
        queueCreateInfos.push_back(info);
    }

    VkPhysicalDeviceFeatures deviceFeatures = {};
    deviceFeatures.samplerAnisotropy = VK_TRUE;

    VkDeviceCreateInfo deviceCreateInfo = { VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO };
    {
        deviceCreateInfo.queueCreateInfoCount = indexCount;
        deviceCreateInfo.pQueueCreateInfos = queueCreateInfos.data();
        deviceCreateInfo.pEnabledFeatures = &deviceFeatures;
        deviceCreateInfo.enabledExtensionCount = 1;
        const char* extensionNames = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
        deviceCreateInfo.ppEnabledExtensionNames = &extensionNames;
    }
    VK_CHECK(vkCreateDevice(context.device.physicalDevice, &deviceCreateInfo, context.allocator, &context.device.logicalDevice));

    vkGetDeviceQueue(context.device.logicalDevice, context.device.graphicsQueueindex, 0, &context.device.graphicsQueue);
    vkGetDeviceQueue(context.device.logicalDevice, context.device.presentQueueIndex, 0, &context.device.presentQueue);
    vkGetDeviceQueue(context.device.logicalDevice, context.device.transferQueueIndex, 0, &context.device.transferQueue);
    vkGetDeviceQueue(context.device.logicalDevice, context.device.computeQueueIndex, 0, &context.device.computeQueue);

    return true;
}

void VulkanDevice::cleanup(VulkanContext& context) {
    context.device.graphicsQueue = nullptr;
    context.device.presentQueue = nullptr;
    context.device.transferQueue = nullptr;
    context.device.computeQueue = nullptr;

    if (context.device.logicalDevice) {
        vkDestroyDevice(context.device.logicalDevice, context.allocator);
        context.device.logicalDevice = nullptr;
    }

    context.device.physicalDevice = nullptr;
}

bool VulkanDevice::selectPhysicalDevice(VulkanContext& context) {
    u32 count = 0;
    VK_CHECK(vkEnumeratePhysicalDevices(context.instance, &count, nullptr));
    if (count == 0) {
        E_FATAL("No devices with Vulkan support found");
        return false;
    }

    std::vector<VkPhysicalDevice> devices;
    devices.resize(count);
    VK_CHECK(vkEnumeratePhysicalDevices(context.instance, &count, devices.data()));

    for (auto i = 0u; i < count; ++i) {
        VkPhysicalDeviceProperties properties;
        vkGetPhysicalDeviceProperties(devices.at(i), &properties);

        VkPhysicalDeviceFeatures features;
        vkGetPhysicalDeviceFeatures(devices.at(i), &features);

        VkPhysicalDeviceMemoryProperties memory;
        vkGetPhysicalDeviceMemoryProperties(devices.at(i), &memory);

        PhysicalDeviceRequirments req;
        req.deviceExtensionNames.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

        PhysicalDeviceQueueFamilyInfo queueInfo;
        const auto result = physicalDeviceMeetsRequirments(devices.at(i), context.surface, properties, features, req, queueInfo, context);

        if (result) {
            E_INFO("selected device: %s", properties.deviceName);
            switch (properties.deviceType) {
                case VK_PHYSICAL_DEVICE_TYPE_OTHER: E_INFO("selected GPU is unknown"); break;
                case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU: E_INFO("selected GPU is integrated"); break;
                case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU: E_INFO("selected GPU is discrete"); break;
                case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU: E_INFO("selected GPU is virtual"); break;
                case VK_PHYSICAL_DEVICE_TYPE_CPU: E_INFO("selected GPU is CPU"); break;
                default: {}
            }
            E_INFO("GPU Driver version: %d.%d.%d",
                VK_VERSION_MAJOR(properties.driverVersion),
                VK_VERSION_MINOR(properties.driverVersion),
                VK_VERSION_PATCH(properties.driverVersion)
            );
            E_INFO("Vulkan API version: %d.%d.%d",
                VK_VERSION_MAJOR(properties.apiVersion),
                VK_VERSION_MINOR(properties.apiVersion),
                VK_VERSION_PATCH(properties.apiVersion)
            );

            for (auto j = 0u; j < memory.memoryHeapCount; ++j) {
                const auto memSize = static_cast<f32>(memory.memoryHeaps[j].size) / 1024.0f / 1024.0f / 1024.0f;
                if (memory.memoryHeaps[j].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) {
                    E_INFO("Local GPU memory: %.2f GiB", memSize);
                } else {
                    E_INFO("Shard system memory: %.2f GiB", memSize);
                }
            }

            context.device.physicalDevice = devices.at(i);
            context.device.graphicsQueueindex = queueInfo.graphicsFamilyIndex;
            context.device.presentQueueIndex = queueInfo.presentFamilyIndex;
            context.device.transferQueueIndex = queueInfo.transferFamilyIndex;
            context.device.computeQueueIndex = queueInfo.computeFamilyIndex;

            context.device.properties = properties;
            context.device.features = features;
            context.device.memory = memory;

            break;
        }
    }
    if (!context.device.physicalDevice) {
        E_ERROR("No suitable physical device found");
        return false;
    }

    return true;
}

bool VulkanDevice::physicalDeviceMeetsRequirments(
    VkPhysicalDevice device,
    VkSurfaceKHR surface,
    const VkPhysicalDeviceProperties& properties,
    const VkPhysicalDeviceFeatures& features,
    const PhysicalDeviceRequirments& requirments,
    PhysicalDeviceQueueFamilyInfo& queueFamilyInfo,
    VulkanContext& context
    ) {
    if (requirments.discreteGpu) {
        if (properties.deviceType != VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) {
            return false;
        }
    }

    u32 count = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &count, nullptr);
    std::vector<VkQueueFamilyProperties> queueFamilies;
    queueFamilies.resize(count);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &count, queueFamilies.data());

    u8 minTransferScore = 255;
    for (auto i = 0u; i < count; ++i) {
        u8 transferScore = 0;

        if (queueFamilies.at(i).queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            queueFamilyInfo.graphicsFamilyIndex = i;
            ++transferScore;
        }

        if (queueFamilies.at(i).queueFlags & VK_QUEUE_COMPUTE_BIT) {
            queueFamilyInfo.computeFamilyIndex = i;
            ++transferScore;
        }

        if (queueFamilies.at(i).queueFlags & VK_QUEUE_TRANSFER_BIT) {
            if (transferScore <= minTransferScore) {
                minTransferScore = transferScore;
                queueFamilyInfo.transferFamilyIndex = i;
            }
        }

        VkBool32 supportsPresentation = false;
        VK_CHECK(vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &supportsPresentation));
        if (supportsPresentation) {
            queueFamilyInfo.presentFamilyIndex = i;
        }
    }

    if ((!requirments.graphics || (requirments.graphics && queueFamilyInfo.graphicsFamilyIndex != PhysicalDeviceQueueFamilyInfo::INVALID_INDEX)) &&
        (!requirments.present || (requirments.present && queueFamilyInfo.presentFamilyIndex != PhysicalDeviceQueueFamilyInfo::INVALID_INDEX)) &&
        (!requirments.compute || (requirments.compute && queueFamilyInfo.computeFamilyIndex != PhysicalDeviceQueueFamilyInfo::INVALID_INDEX)) &&
        (!requirments.transfer || (requirments.transfer && queueFamilyInfo.transferFamilyIndex != PhysicalDeviceQueueFamilyInfo::INVALID_INDEX))) {
        // meets requirments
        auto swapchainSupport = VulkanContext::querySwapchainSupport(device, surface);

        if (swapchainSupport.formats.size() < 1 || swapchainSupport.presentModes.size() < 1) {
            return false;
        }

        context.device.swapchainSupportInfo = swapchainSupport;

        if (!requirments.deviceExtensionNames.empty()) {
            u32 count = 0;
            std::vector<VkExtensionProperties> availableExtensions;
            VK_CHECK(vkEnumerateDeviceExtensionProperties(device, nullptr, &count, nullptr));
            if (count > 0) {
                availableExtensions.resize(count);
                VK_CHECK(vkEnumerateDeviceExtensionProperties(device, nullptr, &count, availableExtensions.data()));

                for (auto j = 0u; j < requirments.deviceExtensionNames.size(); ++j) {
                    bool found = false;
                    for (auto k = 0u; k < count; ++k) {
                        if (stringEqual(requirments.deviceExtensionNames.at(j), availableExtensions.at(k).extensionName)) {
                            found = true;
                            break;
                        }
                    }
                }
            }
        }

        if (requirments.samplerAnisotropy && !features.samplerAnisotropy) {
            return false;
        }

        return true;
    }

    return false;
}

