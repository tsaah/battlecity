#pragma once

#include "VulkanTypes.h"
#include "VulkanImage.h"
#include <v2.h>
#include <vector>

struct VulkanContext final {
    struct SwapchainSupportInfo final {
        VkSurfaceCapabilitiesKHR capabilities;
        std::vector<VkSurfaceFormatKHR> formats;
        std::vector<VkPresentModeKHR> presentModes;
    };

    struct DeviceContext final {
        VkPhysicalDevice physicalDevice;
        VkDevice logicalDevice;
        SwapchainSupportInfo swapchainSupportInfo;
        u32 graphicsQueueindex{ INVALID_INDEX };
        u32 presentQueueIndex{ INVALID_INDEX };
        u32 transferQueueIndex{ INVALID_INDEX };
        u32 computeQueueIndex{ INVALID_INDEX };
        static constexpr u32 INVALID_INDEX{ std::numeric_limits<u32>::max() };

        VkPhysicalDeviceProperties properties;
        VkPhysicalDeviceFeatures features;
        VkPhysicalDeviceMemoryProperties memory;

        VkQueue graphicsQueue;
        VkQueue presentQueue;
        VkQueue transferQueue;
        VkQueue computeQueue;

        VkFormat depthFormat{ VK_FORMAT_UNDEFINED };
    };

    struct SwapchainContextfinal {
        VkSurfaceFormatKHR imageFormat;
        u8 maxnumberOfFramesInFlight{ 0 };
        VkSwapchainKHR swapchain;
        u32 imageCount{ 0 };
        std::vector<VkImage> images;
        std::vector<VkImageView> views;
        VulkanImage depthAttachment;
    };

    Size framebufferSize;
    VkInstance instance;
    VkAllocationCallbacks* allocator{ nullptr };
#if defined(_DEBUG)
    VkDebugUtilsMessengerEXT debugMessenger;
#endif
    VkSurfaceKHR surface;

    DeviceContext device;
    SwapchainContextfinal swapchain;
    u32 imageIndex{ 0 };
    u32 currentFrame{ 0 };
    bool recreatingSwapchain{ false };


    static SwapchainSupportInfo querySwapchainSupport(VkPhysicalDevice aPhysicalDevice, VkSurfaceKHR aSurface);
    static VkFormat detectDepthBuffer(VkPhysicalDevice aPhysicalDevice);
    static i32 findMemoryIndex(VkPhysicalDevice aPhysicalDevice, u32 typeFilteer, u32 propertyFlags);
};