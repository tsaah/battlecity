#pragma once

#include "VulkanTypes.h"
#include "VulkanContext.h"

#include <vector>

struct PlatformWindowState;

class VulkanPlatform final {
public:
    static std::vector<const char*> getRequiredVulkanExtensionNames();
    static VkSurfaceKHR createVulkanSurface(const PlatformWindowState& windowState, const VulkanContext& context);
};