#pragma once
#include "VulkanTypes.h"
#include "VulkanContext.h"

#include <limits>
#include <vector>

class VulkanDevice final {
public:
    struct PhysicalDeviceRequirments {
        bool graphics{ true };
        bool present{ true };
        bool compute{ true };
        bool transfer{ true };
        std::vector<const char*> deviceExtensionNames;
        bool samplerAnisotropy{ true };
        bool discreteGpu{ true };
    };

    struct PhysicalDeviceQueueFamilyInfo {
        u32 graphicsFamilyIndex{ INVALID_INDEX };
        u32 presentFamilyIndex{ INVALID_INDEX };
        u32 computeFamilyIndex{ INVALID_INDEX };
        u32 transferFamilyIndex{ INVALID_INDEX };
        static constexpr u32 INVALID_INDEX{ std::numeric_limits<u32>::max() };
    };

    bool init(VulkanContext& context);
    void cleanup(VulkanContext& context);

private:
    bool selectPhysicalDevice(VulkanContext& context);
    bool physicalDeviceMeetsRequirments(
        VkPhysicalDevice device,
        VkSurfaceKHR surface,
        const VkPhysicalDeviceProperties& properties,
        const VkPhysicalDeviceFeatures& features,
        const PhysicalDeviceRequirments& requirments,
        PhysicalDeviceQueueFamilyInfo& queueFamilyInfo,
        VulkanContext& context
        );

};