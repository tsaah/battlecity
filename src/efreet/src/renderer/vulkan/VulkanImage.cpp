#include "VulkanImage.h"

#include "VulkanContext.h"

#include <LogSystem.h>

void VulkanImage::destroy(const VulkanContext& context, VulkanImage image) {
    if (image.view) {
        vkDestroyImageView(context.device.logicalDevice, image.view, context.allocator);
        image.view = 0;
    }
    if (image.memory) {
        vkFreeMemory(context.device.logicalDevice, image.memory, context.allocator);
        image.memory = 0;
    }
    if (image.image) {
        vkDestroyImage(context.device.logicalDevice, image.image, context.allocator);
        image.image = 0;
    }
}

VulkanImage VulkanImage::create(
    const VulkanContext& context,
    VkImageType imageType,
    Vector2d<u32> asize,
    VkFormat format,
    VkImageTiling imageTiling,
    VkImageUsageFlags usage,
    VkMemoryPropertyFlags memoryFlags,
    bool doCreateView,
    VkImageAspectFlags viewAspectFlags
) {
    VulkanImage image;
    image.size = asize;

    VkImageCreateInfo imageCreateInfo = { VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO };
    {
        imageCreateInfo.imageType = imageType;
        imageCreateInfo.extent.width = asize.width;
        imageCreateInfo.extent.height = asize.height;
        imageCreateInfo.extent.depth = 1; // TODO: 3d textures
        imageCreateInfo.mipLevels = 1; // TODO: miplevels
        imageCreateInfo.arrayLayers = 1; // TODO: array textures
        imageCreateInfo.format = format;
        imageCreateInfo.tiling = imageTiling;
        imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        imageCreateInfo.usage = usage;
        imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT; // TODO: samples
        imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE; // TODO: sharing
    }
    VK_CHECK(vkCreateImage(context.device.logicalDevice, &imageCreateInfo, context.allocator, &image.image));

    VkMemoryRequirements memoryRequirements;
    vkGetImageMemoryRequirements(context.device.logicalDevice, image.image, &memoryRequirements);

    auto memoryType = VulkanContext::findMemoryIndex(context.device.physicalDevice, memoryRequirements.memoryTypeBits, memoryFlags);
    if (memoryType == -1) {
        E_ERROR("required memory type not found");
    }

    VkMemoryAllocateInfo memoryAllocateInfo = { VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO };
    {
        memoryAllocateInfo.allocationSize = memoryRequirements.size;
        memoryAllocateInfo.memoryTypeIndex = memoryType;
    }

    VK_CHECK(vkAllocateMemory(context.device.logicalDevice, &memoryAllocateInfo, context.allocator, &image.memory));

    VK_CHECK(vkBindImageMemory(context.device.logicalDevice, image.image, image.memory, 0));

    if (doCreateView) {
        image.view = createView(context, format, image.image, viewAspectFlags);
    }

    return image;
}

VkImageView VulkanImage::createView(const VulkanContext& context, VkFormat format, VkImage image, VkImageAspectFlags viewAspectFlags) {
    VkImageView view;

    VkImageViewCreateInfo imageViewCreateInfo = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
    {
        imageViewCreateInfo.image = image;
        imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D; // TODO: 3D textures?
        imageViewCreateInfo.format = format;
        imageViewCreateInfo.subresourceRange.aspectMask = viewAspectFlags;
        imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
        imageViewCreateInfo.subresourceRange.levelCount = 1;
        imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
        imageViewCreateInfo.subresourceRange.layerCount = 1;
    }

    VK_CHECK(vkCreateImageView(context.device.logicalDevice, &imageViewCreateInfo, context.allocator, &view));

    return view;
}