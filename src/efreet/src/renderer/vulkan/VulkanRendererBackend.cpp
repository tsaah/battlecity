#include "VulkanRendererBackend.h"
#include "VulkanPlatform.h"

#include <StringUtils.h>

#include <vector>

VulkanRendererBackend::~VulkanRendererBackend() {
    // TODO: destroy vulkan stuff
}

bool VulkanRendererBackend::init(const char* applicationName, const PlatformWindowState& windowState) {
    if (!createInstance(applicationName)) {
        return false;
    }
    createDebugger();

    context_.surface = VulkanPlatform::createVulkanSurface(windowState, context_);
    E_ASSERT(context_.surface);

    if (!vulkanDevice_.init(context_)) {
        return false;
    }

    if (!swapchain_.create(context_)) {
        return false;
    }

    E_INFO("VulkanRendererBackend initialized");

    return true;
}

void VulkanRendererBackend::resize(v2i size) {

}

bool VulkanRendererBackend::beginFrame(f32 dt) {
    return true;
}

bool VulkanRendererBackend::endFrame(f32 dt) {
    return true;
}

bool VulkanRendererBackend::createInstance(const char* applicationName) {
    VkApplicationInfo applicationInfo = { VK_STRUCTURE_TYPE_APPLICATION_INFO };
    {
        applicationInfo.apiVersion = VK_API_VERSION_1_2;
        applicationInfo.pApplicationName = applicationName;
        applicationInfo.applicationVersion = VK_MAKE_VERSION(0, 0, 1);
        applicationInfo.pEngineName = "Efreet";
        applicationInfo.engineVersion = VK_MAKE_VERSION(0, 0, 1);
    }

    std::vector<const char*> requiredExtensions;
    requiredExtensions.reserve(5);
    {
        requiredExtensions.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
        for (const auto& e: VulkanPlatform::getRequiredVulkanExtensionNames()) {
            requiredExtensions.push_back(e);
        }
#if defined(_DEBUG)
        requiredExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        E_INFO("Vulkan required extensions (%u):", requiredExtensions.size());
        for (u8 i = 0; i < requiredExtensions.size(); ++i) {
            E_INFO("\t%s", requiredExtensions.at(i));
        }
#endif
    }

    std::vector<const char*> requiredValidationLayers;
    requiredValidationLayers.reserve(5);
    {
#if defined(_DEBUG)
        requiredValidationLayers.push_back("VK_LAYER_KHRONOS_validation");
        u32 availableLayerCount = 0;
        VK_CHECK(vkEnumerateInstanceLayerProperties(&availableLayerCount, nullptr));
        std::vector<VkLayerProperties> availableLayers;
        availableLayers.resize(availableLayerCount);
        VK_CHECK(vkEnumerateInstanceLayerProperties(&availableLayerCount, availableLayers.data()));

        for (auto i = 0u; i < requiredValidationLayers.size(); ++i) {
            bool found = false;
            for (auto j = 0u; j < availableLayerCount; ++j) {
                if (stringEqual(requiredValidationLayers.at(i), availableLayers.at(j).layerName)) {
                    found = true;
                    E_INFO("Required validation layer %s was found", requiredValidationLayers.at(i));
                    break;
                }
            }
            if (!found) {
                E_FATAL("Required validation layer %s was not found", requiredValidationLayers.at(i));
                return false;
            }
        }
#endif
    }

    VkInstanceCreateInfo instanceCreateInfo = { VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO };
    {
        instanceCreateInfo.pApplicationInfo = &applicationInfo;
        instanceCreateInfo.enabledExtensionCount = requiredExtensions.size();
        instanceCreateInfo.ppEnabledExtensionNames = requiredExtensions.data();
        instanceCreateInfo.enabledLayerCount = requiredValidationLayers.size();
        instanceCreateInfo.ppEnabledLayerNames = requiredValidationLayers.data();
    }

    VK_CHECK(vkCreateInstance(&instanceCreateInfo, context_.allocator, &context_.instance));

    E_INFO("Vulkan instance created");
    return true;
}

void VulkanRendererBackend::createDebugger() {
#if defined(_DEBUG)
    VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo = { VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT };
    {
        debugCreateInfo.messageSeverity =
            // VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
            // VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;
        debugCreateInfo.messageType =
            VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT |
            VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
        debugCreateInfo.pfnUserCallback = &VulkanRendererBackend::vkDebugCallback;
        debugCreateInfo.pUserData = this;
    }

    auto function_vkCreateDebugUtilsMessengerEXT = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(context_.instance, "vkCreateDebugUtilsMessengerEXT"));
    E_ASSERT(function_vkCreateDebugUtilsMessengerEXT);
    VK_CHECK(function_vkCreateDebugUtilsMessengerEXT(context_.instance, &debugCreateInfo, context_.allocator, &context_.debugMessenger));

    E_INFO("Vulkan debugger created");
#endif
}

VKAPI_ATTR VkBool32 VKAPI_CALL VulkanRendererBackend::vkDebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageTypes, const VkDebugUtilsMessengerCallbackDataEXT* callbackData, void* userData) {
    switch (messageSeverity) {
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT:
            E_ERROR(callbackData->pMessage);
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT:
            E_WARN(callbackData->pMessage);
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT:
            E_INFO(callbackData->pMessage);
            break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
            E_TRACE(callbackData->pMessage);
            break;
        default:
            break;
    }
    return VK_FALSE;
}