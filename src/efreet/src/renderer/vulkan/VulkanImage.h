#pragma once

#include "VulkanTypes.h"
#include <v2.h>

struct VulkanContext;

struct VulkanImage final {
    VkImage image;
    VkDeviceMemory memory;
    VkImageView view;
    Vector2d<u32> size;

    static void destroy(const VulkanContext& context, VulkanImage image);
    static VulkanImage create(
        const VulkanContext& context,
        VkImageType imageType,
        Vector2d<u32> asize,
        VkFormat format,
        VkImageTiling imageTiling,
        VkImageUsageFlags usage,
        VkMemoryPropertyFlags memoryFlags,
        bool createView,
        VkImageAspectFlags viewAspectFlags
    );
    static VkImageView createView(const VulkanContext& context, VkFormat format, VkImage image, VkImageAspectFlags viewAspectFlags);
};