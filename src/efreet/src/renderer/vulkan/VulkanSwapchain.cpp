#include "VulkanSwapchain.h"
#include "VulkanImage.h"

#include <LogSystem.h>
#include <MathUtils.h>

#include <limits>

static const u32 INVALID_INDEX = std::numeric_limits<u32>::max();

bool VulkanSwapchain::create(VulkanContext& context) {
    context.swapchain.images.reserve(3);
    context.swapchain.views.reserve(3);

    const auto size = context.framebufferSize.typeCast<u32>();
    VkExtent2D swapchainExtent = { size.width, size.height };
    context.swapchain.maxnumberOfFramesInFlight = 2;


    bool found = false;
    for (const auto& f: context.device.swapchainSupportInfo.formats) {
        if (f.format == VK_FORMAT_B8G8R8A8_UNORM && f.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
            context.swapchain.imageFormat = f;
            found = true;
            break;
        }
    }

    if (!found) {
        E_ASSERT(!context.device.swapchainSupportInfo.formats.empty());
        context.swapchain.imageFormat = context.device.swapchainSupportInfo.formats.at(0);
    }

    auto presentMode = VK_PRESENT_MODE_FIFO_KHR;
    for (const auto& m: context.device.swapchainSupportInfo.presentModes) { // TODO: better selection of modes for VSYNC
        // if (m == VK_PRESENT_MODE_MAILBOX_KHR) { // vsync
        //     presentMode = m;
        //     break;
        // }
        if (m == VK_PRESENT_MODE_IMMEDIATE_KHR) { // no vsync
            presentMode = m;
            break;
        }
    }

    context.device.swapchainSupportInfo = VulkanContext::querySwapchainSupport(context.device.physicalDevice, context.surface);

    if (context.device.swapchainSupportInfo.capabilities.currentExtent.width != UINT32_MAX) {
        swapchainExtent = context.device.swapchainSupportInfo.capabilities.currentExtent;
    }

    VkExtent2D min = context.device.swapchainSupportInfo.capabilities.minImageExtent;
    VkExtent2D max = context.device.swapchainSupportInfo.capabilities.maxImageExtent;
    swapchainExtent.width = clamp(swapchainExtent.width, min.width, max.width);
    swapchainExtent.height = clamp(swapchainExtent.height, min.height, max.height);

    auto imageCount = context.device.swapchainSupportInfo.capabilities.minImageCount + 1;
    if (context.device.swapchainSupportInfo.capabilities.maxImageCount > 0 && imageCount > context.device.swapchainSupportInfo.capabilities.maxImageCount) {
        imageCount = context.device.swapchainSupportInfo.capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR swapchainCreateInfo = { VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR };
    {
        swapchainCreateInfo.surface = context.surface;
        swapchainCreateInfo.minImageCount = imageCount;
        swapchainCreateInfo.imageFormat = context.swapchain.imageFormat.format;
        swapchainCreateInfo.imageColorSpace = context.swapchain.imageFormat.colorSpace;
        swapchainCreateInfo.imageExtent = swapchainExtent;
        swapchainCreateInfo.imageArrayLayers = 1;
        swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        swapchainCreateInfo.preTransform = context.device.swapchainSupportInfo.capabilities.currentTransform;
        swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        swapchainCreateInfo.presentMode = presentMode;
        swapchainCreateInfo.clipped = VK_TRUE;

        if (context.device.graphicsQueueindex != context.device.presentQueueIndex) {
            u32 queueFamilyIdices[] = {
                context.device.graphicsQueueindex,
                context.device.presentQueueIndex,
            };
            swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
            swapchainCreateInfo.queueFamilyIndexCount = 2;
            swapchainCreateInfo.pQueueFamilyIndices = queueFamilyIdices;
        } else {
            swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
            swapchainCreateInfo.queueFamilyIndexCount = 0;
            swapchainCreateInfo.pQueueFamilyIndices = nullptr;
        }
    }

    VK_CHECK(vkCreateSwapchainKHR(context.device.logicalDevice, &swapchainCreateInfo, context.allocator, &context.swapchain.swapchain));

    context.currentFrame = 0;
    context.swapchain.imageCount = 0;
    VK_CHECK(vkGetSwapchainImagesKHR(context.device.logicalDevice, context.swapchain.swapchain, &context.swapchain.imageCount, nullptr));
    context.swapchain.images.resize(context.swapchain.imageCount);
    context.swapchain.views.resize(context.swapchain.imageCount);
    VK_CHECK(vkGetSwapchainImagesKHR(context.device.logicalDevice, context.swapchain.swapchain, &context.swapchain.imageCount, context.swapchain.images.data()));

    auto j = 0;
    for (const auto& i: context.swapchain.images) {
        VkImageViewCreateInfo imageViewCreateInfo = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
        {
            imageViewCreateInfo.image = i;
            imageViewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
            imageViewCreateInfo.format = context.swapchain.imageFormat.format;
            imageViewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
            imageViewCreateInfo.subresourceRange.levelCount = 1;
            imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
            imageViewCreateInfo.subresourceRange.layerCount = 1;
        }
        VK_CHECK(vkCreateImageView(context.device.logicalDevice, &imageViewCreateInfo, context.allocator, context.swapchain.views.data() + j++));
    }

    context.device.depthFormat = VulkanContext::detectDepthBuffer(context.device.physicalDevice);
    if (context.device.depthFormat == VK_FORMAT_UNDEFINED) {
        E_FATAL("Failed to detect suitable depth format");
        return false;
    }

    context.swapchain.depthAttachment = VulkanImage::create(
        context,
        VK_IMAGE_TYPE_2D,
        { swapchainExtent.width, swapchainExtent.height },
        context.device.depthFormat,
        VK_IMAGE_TILING_OPTIMAL,
        VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT,
        true,
        VK_IMAGE_ASPECT_DEPTH_BIT
        );

    E_INFO("Vulkan swapchain created successfully");

    return true;
}

void VulkanSwapchain::recreate(VulkanContext& context) {
    destroy(context);
    create(context);
}

void VulkanSwapchain::destroy(VulkanContext& context) {
    VulkanImage::destroy(context, context.swapchain.depthAttachment);

    for (auto i = 0u; i < context.swapchain.imageCount; ++i) {
        vkDestroyImageView(context.device.logicalDevice, context.swapchain.views.at(i), context.allocator);
    }

    vkDestroySwapchainKHR(context.device.logicalDevice, context.swapchain.swapchain, context.allocator);
}

u32 VulkanSwapchain::getNextImageIndex(VulkanContext& context, u64 timeoutMs, VkSemaphore imageAvailableSemaphore, VkFence fence) {
    u32 imageIndex = INVALID_INDEX;
    auto result = vkAcquireNextImageKHR(context.device.logicalDevice, context.swapchain.swapchain, timeoutMs, imageAvailableSemaphore, fence, &imageIndex);

    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        recreate(context);
        return false;
    } else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR) {
        E_FATAL("Failed to aquire swapchain image");
        return INVALID_INDEX;
    }

    return imageIndex;
}

void VulkanSwapchain::present(VulkanContext& context, VkQueue graphicsQueue, VkQueue presentQueue, VkSemaphore renderCompleteSemaphore, u32 presentImageIndex) {
    VkPresentInfoKHR presentInfo = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR };
    {
        presentInfo.waitSemaphoreCount = 1;
        presentInfo.pWaitSemaphores = &renderCompleteSemaphore;
        presentInfo.swapchainCount = 1;
        presentInfo.pSwapchains = &context.swapchain.swapchain;
        presentInfo.pImageIndices = &presentImageIndex;
    }

    auto result = vkQueuePresentKHR(presentQueue, &presentInfo);
    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR) {
        recreate(context);
    } else if (result != VK_SUCCESS) {
        E_FATAL("Failed to present swapchain image");
    }
}

