#include "../IRendererBackend.h"
#include "VulkanTypes.h"
#include "VulkanContext.h"
#include "VulkanDevice.h"
#include "VulkanSwapchain.h"

class VulkanRendererBackend final: public IRendererBackend {
public:
    ~VulkanRendererBackend() override;
    bool init(const char* applicationName, const PlatformWindowState& windowState) override;
    void resize(v2i size) override;
    bool beginFrame(f32 dt) override;
    bool endFrame(f32 dt) override;

private:
    VulkanContext context_;
    VulkanDevice vulkanDevice_;
    VulkanSwapchain swapchain_;

    bool createInstance(const char* applicationName);
    void createDebugger();

    static VKAPI_ATTR VkBool32 VKAPI_CALL vkDebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageTypes, const VkDebugUtilsMessengerCallbackDataEXT* callbackData, void* userData);
};