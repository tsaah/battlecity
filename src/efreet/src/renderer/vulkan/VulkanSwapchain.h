#pragma once

#include "VulkanTypes.h"
#include "VulkanContext.h"

#include <vector>

class VulkanSwapchain final {
public:
    bool create(VulkanContext& context);
    void recreate(VulkanContext& context);
    void destroy(VulkanContext& context);
    u32 getNextImageIndex(VulkanContext& context, u64 timeoutMs, VkSemaphore imageAvailableSemafore, VkFence fence);
    void present(VulkanContext& context, VkQueue graphicsQueue, VkQueue presentQueue, VkSemaphore renderCompleteSemaphore, u32 presentImageIndex);
};