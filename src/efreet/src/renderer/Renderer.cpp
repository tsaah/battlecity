#include "Renderer.h"

#include <LogSystem.h>
#include <win32/PlatformWindowState.inl>
#include <linux/PlatformWindowState.inl>

bool Renderer::init(const char* applicationName, const PlatformWindowState& windowState, IRendererBackendFactoryPtr&& backendFactory) {
    rendererBackendFactory_ = std::move(backendFactory);
    E_ASSERT(rendererBackendFactory_);
    backend_ = rendererBackendFactory_->create(RendererBackendType::VULKAN);
    E_ASSERT(backend_);
    if (!backend_->init(applicationName, windowState)) {
        E_FATAL("Failed to initialize Renderer Backend");
        return false;
    }
    return true;
}

void Renderer::onResized(Size size) {

}

bool Renderer::drawFrame(const RenderPacketPtr& packet) {
    E_ASSERT(packet);
    const auto dt = packet->dt_;
    if (backend_->beginFrame(dt)) {
        if (!backend_->endFrame(dt)) {
            E_ERROR("Renderer endFrame() failed");
            return false;
        }
    }
    return true;
}
