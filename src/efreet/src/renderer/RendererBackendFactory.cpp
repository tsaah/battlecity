#include "RendererBackendFactory.h"
#include "vulkan/VulkanRendererBackend.h"

std::unique_ptr<IRendererBackend> RendererBackendFactory::create(RendererBackendType type) {
    std::unique_ptr<IRendererBackend> backend;
    if (type == RendererBackendType::VULKAN) {
        backend = std::make_unique<VulkanRendererBackend>();
    } else if (type == RendererBackendType::OPENGL) {
        // backend = std::make_unique<OpenGLRendererBackend>();
    } else if (type == RendererBackendType::DIRECTX) {
        // backend = std::make_unique<DirectXRendererBackend>();
    }
    return backend;
}