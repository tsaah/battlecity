#include "../PlatformMemory.h"

#include <platform_detection.h>

#if EFREET_PLATFORM_WINDOWS

#include <Windows.h>

void PlatformMemory::free(void* address, bool aligned) const {
	::VirtualFree(address, 0, MEM_RELEASE);
}

void* PlatformMemory::alloc(size_t size, bool aligned) const {
	return ::VirtualAlloc(nullptr, size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
}

void* PlatformMemory::zero(void* address, size_t size) const {
	return ::memset(address, 0, size);
}

void* PlatformMemory::copy(const void* source, size_t size, void* destination) const {
	return ::memcpy(destination, source, size);
}

#endif