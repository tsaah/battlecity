#include "../PlatformWindow.h"

#include <LogSystem.h>

#if EFREET_PLATFORM_WINDOWS

// #include <core/Input.h>

static ::LRESULT CALLBACK windowEventHandler(::HWND windowHandle, u32 event, ::WPARAM wParam, ::LPARAM lParam) {
    PlatformWindow* window{ nullptr };
    if (event == WM_NCCREATE) {
        window = static_cast<PlatformWindow*>(reinterpret_cast<::LPCREATESTRUCT>(lParam)->lpCreateParams);
        ::SetLastError(0);
        if (!::SetWindowLongPtrA(windowHandle, GWLP_USERDATA, reinterpret_cast<::LONG_PTR>(reinterpret_cast<::LPCREATESTRUCT>(lParam)->lpCreateParams))) {
            if (::GetLastError() != 0) {
                return FALSE;
            }
        }
    } else {
        window = reinterpret_cast<PlatformWindow*>(::GetWindowLongPtrA(windowHandle, GWLP_USERDATA));
    }

    if (window != nullptr) {
        if (event == WM_CLOSE || event == WM_DESTROY) {
            window->close();
        }
    }

    return ::DefWindowProcA(windowHandle, event, wParam, lParam);
}

PlatformWindow::~PlatformWindow() {
    if (state_.hWnd_) {
        ::DestroyWindow(state_.hWnd_);
        state_.hWnd_ = nullptr;
    }
}

bool PlatformWindow::create(const Config& config) {
    E_ASSERT(state_.hInstance_ == nullptr);
    state_.hInstance_ = ::GetModuleHandleA(nullptr);
    E_ASSERT(state_.hInstance_);
    E_ASSERT(state_.hWnd_ == nullptr);
    E_ASSERT(::GetLastError() == 0);

    ::WNDCLASSA windowClass = {};
    windowClass.style = CS_DBLCLKS;
    windowClass.lpfnWndProc = windowEventHandler; // DefWindowProcA; // PlatformWindow::s_eventRouter;
    windowClass.hInstance = state_.hInstance_;
    windowClass.hIcon = LoadIcon(nullptr, IDI_APPLICATION);
    windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
    windowClass.lpszClassName = "EfreetWindowClass";
    E_ASSERT(::GetLastError() == 0);

    if (!::RegisterClassA(&windowClass)) {
        const auto error = ::GetLastError();
        E_FATAL("Window registration failed. Last error: %d", error);
        ::MessageBoxA(nullptr, "Window registration failed", "Error", MB_ICONEXCLAMATION | MB_OK);
        return FALSE;
    }
    E_ASSERT(::GetLastError() == 0);

    const u32 clientX = config.rect.x();
    const u32 clientY = config.rect.y();
    const u32 clientWidth = config.rect.width();
    const u32 clientHeight = config.rect.height();

    u32 windowStyle = WS_OVERLAPPED | WS_SYSMENU | WS_CAPTION;
    const u32 windowStyleEx = WS_EX_APPWINDOW;

    windowStyle |= WS_MAXIMIZEBOX;
    windowStyle |= WS_MINIMIZEBOX;
    windowStyle |= WS_THICKFRAME;

    ::RECT borderRect = { 0, 0, 0, 0 };
    if (!::AdjustWindowRectEx(&borderRect, windowStyle, 0, windowStyleEx)) {
        const auto error = ::GetLastError();
        E_FATAL("Adjust window rect failed. Last error: %d", error);
        ::MessageBoxA(nullptr, "Adjust window rect failed", "Error", MB_ICONEXCLAMATION | MB_OK);
        return FALSE;
    }
    E_ASSERT(::GetLastError() == 0);

    const u32 windowX = clientX + borderRect.left;
    const u32 windowY = clientY + borderRect.top;
    const u32 windowWidth = clientWidth + borderRect.right - borderRect.left;
    const u32 windowHeight = clientHeight + borderRect.bottom - borderRect.top;

    state_.hWnd_ = ::CreateWindowExA(
        windowStyleEx,
        windowClass.lpszClassName,
        config.title,
        windowStyle,
        windowX, windowY, windowWidth, windowHeight,
        nullptr, nullptr, state_.hInstance_, reinterpret_cast<void*>(this));
    E_ASSERT(::GetLastError() == 0);
    if (state_.hWnd_ == nullptr) {
        const auto error = ::GetLastError();
        E_FATAL("Window creation failed. Last error: %d", error);
        ::MessageBoxA(nullptr, "Window creation failed", "Error", MB_ICONEXCLAMATION | MB_OK);
        return FALSE;
    }

    bool shouldActivate = 1;
    const i32 showWindowCommandFlags = shouldActivate ? SW_SHOW : SW_SHOWNOACTIVATE;
    ::ShowWindow(state_.hWnd_, showWindowCommandFlags);
    E_ASSERT(::GetLastError() == 0);

    isOpen_ = true;

    return TRUE;
}

void PlatformWindow::close() {
    isOpen_ = false;
}

bool PlatformWindow::isOpen() const {
    return isOpen_;
}

// ::LRESULT CALLBACK PlatformWindow::s_eventRouter(::HWND windowHandle, u32 event, ::WPARAM wParam, ::LPARAM lParam) {
//     Window* window{ nullptr };
//     if (event == WM_NCCREATE) {
//         window = static_cast<Window*>(reinterpret_cast<::LPCREATESTRUCT>(lParam)->lpCreateParams);
//         ::SetLastError(0);
//         if (!::SetWindowLongPtrA(windowHandle, GWLP_USERDATA, reinterpret_cast<::LONG_PTR>(reinterpret_cast<::LPCREATESTRUCT>(lParam)->lpCreateParams))) {
//             if (::GetLastError() != 0) {
//                 return FALSE;
//             }
//         }
//     } else {
//         window = reinterpret_cast<Window*>(::GetWindowLongPtrA(windowHandle, GWLP_USERDATA));
//     }

//     if (window != nullptr) {
//         return window->eventHandler(windowHandle, event, wParam, lParam);
//     }

//     return ::DefWindowProcA(windowHandle, event, wParam, lParam);
// }

// ::LRESULT PlatformWindow::eventHandler(::HWND windowHandle, u32 event, ::WPARAM wParam, ::LPARAM lParam) {
//     switch (event) {
//         case WM_ERASEBKGND:
//             // Notify the OS that erasing will be handled by the application to prevent flicker.
//             return 1;
//         case WM_DESTROY:
//             ::PostQuitMessage(0);
//             return 0;
//         case WM_CLOSE:
//             // TODO: Fire an event for the application to quit.
//             return 0;
//         case WM_SIZE: {
//             // Get the updated size.
//             //  RECT r;
//             // GetClientRect(hwnd, &r);
//             // u32 width = r.right - r.left;
//             // u32 height = r.bottom - r.top;

//             // TODO: Fire an event for window resize.
//         } break;
//         case WM_KEYDOWN:
//         case WM_SYSKEYDOWN:
//         case WM_KEYUP:
//         case WM_SYSKEYUP: {
//             const b8 pressed = (event == WM_KEYDOWN || event == WM_SYSKEYDOWN);
//             const auto k = static_cast<core::Input::Keys>(static_cast<u16>(wParam));
//             E_Input.processKey(k, pressed);
//         } break;
//         case WM_MOUSEMOVE: {
//             const v2i pos(GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
//             E_Input.processMouseMove(pos);
//         } break;
//         case WM_MOUSEWHEEL: {
//             i32 zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
//             if (zDelta != 0) {
//                 zDelta = (zDelta < 0) ? -1 : 1;
//                 E_Input.processMouseWheel(zDelta);
//             }
//         } break;
//         case WM_LBUTTONDOWN:
//         case WM_LBUTTONUP: {
//             const b8 pressed = (event == WM_LBUTTONDOWN || event == WM_RBUTTONDOWN || event == WM_MBUTTONDOWN);
//             E_Input.processButton(E_Buttons::LEFT, pressed);
//         } break;
//         case WM_MBUTTONDOWN:
//         case WM_MBUTTONUP: {
//             const b8 pressed = (event == WM_LBUTTONDOWN || event == WM_RBUTTONDOWN || event == WM_MBUTTONDOWN);
//             E_Input.processButton(E_Buttons::MIDDLE, pressed);
//         } break;
//         case WM_RBUTTONDOWN:
//         case WM_RBUTTONUP: {
//             const b8 pressed = (event == WM_LBUTTONDOWN || event == WM_RBUTTONDOWN || event == WM_MBUTTONDOWN);
//             E_Input.processButton(E_Buttons::RIGHT, pressed);
//         } break;
//     }
//     return ::DefWindowProcA(windowHandle, event, wParam, lParam);
// }

#endif