#include <platform_detection.h>

#if EFREET_PLATFORM_WINDOWS

#include "../PlatformEventProcessor.h"
#include "../Application.h"

#include <basic_types.h>

#include <Windows.h>

bool PlatformEventProcessor::processEvents(Application& application) { // TODO:: pass application here to controll it
    ::MSG message = {};
    while (::PeekMessageA(&message, nullptr, 0, 0, PM_REMOVE)) {
        switch (message.message) {
            case WM_QUIT: {
                application.stop();
                // m_state.running = false;
            } break;

            case WM_SYSKEYDOWN:
            case WM_SYSKEYUP:
            case WM_KEYDOWN:
            case WM_KEYUP: {
                const auto keyCode{ static_cast<u32>(message.wParam) };
                const auto wasDown{ ((message.lParam & (1 << 30)) != 0) };
                const auto isDown{ ((message.lParam & (1 << 31)) == 0) };
                application.processKey(keyCode, isDown, wasDown); // TODO: consider clt ctrl shift keys and all other keys shold be mapped
                // if (wasDown != isDown) {
                //     if(keyCode == 'W') {
                //         processKeyboardMessage(keyboardController.moveUp, isDown);
                //     } else if (keyCode == 'A') {
                //         processKeyboardMessage(keyboardController.moveLeft, isDown);
                //     } else if (keyCode == 'S') {
                //         processKeyboardMessage(keyboardController.moveDown, isDown);
                //     } else if (keyCode == 'D') {
                //         processKeyboardMessage(keyboardController.moveRight, isDown);
                //     } else if (keyCode == 'Q') {
                //         processKeyboardMessage(keyboardController.leftShoulder, isDown);
                //     } else if (keyCode == 'E') {
                //         processKeyboardMessage(keyboardController.rightShoulder, isDown);
                //     } else if (keyCode == VK_UP) {
                //         processKeyboardMessage(keyboardController.actionUp, isDown);
                //     } else if (keyCode == VK_LEFT) {
                //         processKeyboardMessage(keyboardController.actionLeft, isDown);
                //     } else if (keyCode == VK_DOWN) {
                //         processKeyboardMessage(keyboardController.actionDown, isDown);
                //     } else if (keyCode == VK_RIGHT) {
                //         processKeyboardMessage(keyboardController.actionRight, isDown);
                //     } else if (keyCode == VK_ESCAPE) {
                //         processKeyboardMessage(keyboardController.start, isDown);
                //     } else if (keyCode == VK_SPACE) {
                //         processKeyboardMessage(keyboardController.back, isDown);
                //     }
                //     if (isDown) {
                //         bool altKeyDown{ ((message.lParam & (1 << 29)) != 0) };
                //         if (keyCode == VK_F4 && altKeyDown) {
                //             m_state.running = false;
                //         }
                //         if (keyCode == VK_RETURN && altKeyDown) {
                //             if (message.hwnd) {
                //                 m_window.toggleFullscreen();
                //             }
                //         }
                //     }
                // }
            } break;

            default: {
                ::TranslateMessage(&message);
                ::DispatchMessageA(&message);
            } break;
        }
    }

    return true;
}

#endif