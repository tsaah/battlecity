#pragma once

#include <platform_detection.h>

#if EFREET_PLATFORM_WINDOWS

#include <rect.h>

#include <Windows.h>

struct PlatformWindowState final {
    HINSTANCE hInstance_{ nullptr };
    HWND hWnd_{ nullptr };
    Rect rect;
};

#endif