#include "../Game.h"

#include <platform_detection.h>

#if EFREET_PLATFORM_WINDOWS

#include <LogSystem.h>

#include <Windows.h>

bool Game::load(const char* dll) {
    auto* module = LoadLibraryA(dll); // CONSIDER: cleaning module
    if (!module) {
        E_FATAL("Failed to load game library %s", dll);
        return false;
    }

    auto* createGameInstanceFunction = reinterpret_cast<decltype(&createGameInstance)>(GetProcAddress(module, "createGameInstance"));
    if (!createGameInstanceFunction) {
        E_FATAL("Failed to load createGameInstance function from library %s", dll);
        return false;
    }

    game_.reset(createGameInstanceFunction());
    if (!game_) {
        E_FATAL("Failed to load create game from library %s", dll);
        return false;
    }

    return true;
}

bool Game::init() {
    return game_->init();
}

bool Game::update() {
    return game_->update();
}

#endif