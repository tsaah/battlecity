#pragma once

#include "Game.h"
#include "PlatformWindow.h"
#include "PlatformEventProcessor.h"
#include "PlatformMemory.h"
#include "renderer/Renderer.h"

#include <basic_types.h>

class Application final {
public:
    bool init(int argc, char** argv);
    bool exec();

    void stop();
    void processKey(u32 key, bool isDown, bool wasDown);

private:
    bool isRunning_{ false };
    bool isSuspended_{ false };
    PlatformWindow window_;
    PlatformEventProcessor eventProcessor_;
    PlatformMemory memory_;

    void* persistentMemory_{ nullptr };
    void* transientMemory_{ nullptr };
    Game game_;
    Renderer renderer_;
};