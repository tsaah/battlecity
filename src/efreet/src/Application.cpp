#include "Application.h"

#include <basic_types.h>
#include <LogSystem.h>
#include "renderer/RendererBackendFactory.h"

bool Application::init(int argc, char** argv) {
    if (!game_.load("sandbox.dll")) {
        return false;
    }

    if (!game_.init()) {
        E_FATAL("Failed to init a game");
        return false;
    }

    if (!window_.create({ "title", { 100, 100, 400, 200 } })) {
        E_FATAL("Failed to create window");
        return false;
    }

    if (!renderer_.init("title", window_.state(), std::make_unique<RendererBackendFactory>())) {
        E_FATAL("Failed to init renderer");
        return false;
    }

    persistentMemory_ = memory_.alloc(megabytes(1), true);
    if (!persistentMemory_) {
        E_FATAL("Failed to allocate persistant memory");
        return false;
    }

    transientMemory_ = memory_.alloc(megabytes(1), true);
    if (!transientMemory_) {
        E_FATAL("Failed to allocate transient memory");
        return false;
    }

    isRunning_ = true;
    return true;
}

bool Application::exec() {
    while (isRunning_ && window_.isOpen()) {
        if (!eventProcessor_.processEvents(*this)) {
            isRunning_ = false;
        }

        if (!isSuspended_) {
            if (!game_.update()) {
                E_ERROR("Failed to update a game");
                return false;
            }
        }
    }

    return true;
}

void Application::stop() {
    isRunning_ = false;
}

void Application::processKey(u32 key, bool isDown, bool wasDown) {
    E_INFO("key event %u %u %u", key, isDown, wasDown);
    if (key == 27 && isDown) {
        stop();
    }
}