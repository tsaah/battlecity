#pragma once

#include <IGame.h>

#include <memory>

class Game final: public IGame {
public:
    bool load(const char* dll);
    bool init() override;
    bool update() override;

private:
    std::unique_ptr<IGame> game_;
};