#pragma once

#if defined(SANDBOX_EXPORT)
    #ifdef _MSC_VER
        #define API_SANDBOX __declspec(dllexport)
    #else
        #define API_SANDBOX __attribute((visibility("default")))
    #endif
#else
    #ifdef _MSC_VER
        #define API_SANDBOX __declspec(dllimport)
    #else
        #define API_SANDBOX
    #endif
#endif
