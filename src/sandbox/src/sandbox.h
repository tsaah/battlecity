#include "sandbox_export.h"

#include <IGame.h>

class Sandbox: public IGame {
public:
    bool init() override;
    bool update() override;
};

IGame* createGameInstance() {
    return new Sandbox;
}