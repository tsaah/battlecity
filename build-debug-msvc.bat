@echo off
set CC=clang
set CXX=/clang++

cmake -S . -B ./build -DCMAKE_VERBOSE_MAKEFILE=TRUE -DCMAKE_BUILD_TYPE=Debug

cmake --build ./build
